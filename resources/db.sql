create schema tester ;

create table if not exists tester.tablea (a numeric, b numeric, c numeric, d numeric, e numeric, primary key (a,b), foreign key (c, d) references tester.tableb (c, d));

create table if not exists tester.tableb (c numeric, d numeric, p numeric, primary key (c, d));

create table if not exists tester.tablec (c numeric, d numeric, g numeric, foreign key (c, d) references tester.tableb (c, d), foreign key (g) references tester.tabled (g));

create table if not exists tester.tabled (g numeric, h numeric, primary key (g));

create table if not exists tester.tablee (column_one text, column_two text);

