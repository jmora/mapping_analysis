package eu.optique.api.component.omm.analysis.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author jmora
 * 
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ SQLQueryTest.class, BodySyntaxTest.class, FrontierSyntaxTest.class, HeadSyntaxTest.class,
		DisambiguatorTest.class, // HeadSemanticsBasicTest.class, SQLRepairTest.class,
		SemanticHeadCheckTest01.class, SemanticHeadCheckTest02.class, SemanticHeadCheckTest03.class, SemanticHeadCheckTest04.class, SemanticHeadCheckTest05.class, SemanticHeadCheckTest06.class,
		BatchAnalysisTest.class })
public class TestSuite {

}
