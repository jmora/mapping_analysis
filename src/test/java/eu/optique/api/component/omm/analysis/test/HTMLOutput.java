package eu.optique.api.component.omm.analysis.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import net.sf.jsqlparser.JSQLParserException;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;

import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedSingleResult;
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper;
import eu.optique.api.component.omm.analysis.sqlapi.SQLPartialAPI;
import eu.optique.api.component.omm.analysis.syntax.HeadChecker;
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class HTMLOutput {

	private ArrayList<TriplesMap> mappings;
	private HashMap<String, Set<String>> signature;
	private ArrayList<String> problemsFound;

	@Before
	public void prepare () throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException, InvalidSchemaSpecificationException,
			OWLOntologyCreationException, InvalidR2RMLMappingException {
		TerminalHelper helper = new TerminalHelper();
		this.mappings = new ArrayList<TriplesMap>(helper.getMappings("resources/mappings.ttl"));
		OWLOntology ontology = helper.getOntology("resources/ontology.ttl");
		this.signature = SyntacticAnalyserImpl.getSignature(ontology);
		this.problemsFound = new ArrayList<String>();
		for (int i = 0; i < this.mappings.size(); i++)
			try {
				new SQLPartialAPI(this.mappings.get(i));
				new HashSet<Integer>();
				for (MappingAnalysisOrderedSingleResult result : HeadChecker.check(this.mappings, i, this.signature))
					this.problemsFound.add(result.toHTML());
			} catch (JSQLParserException e) {
				System.out.println("Parser error for mapping: " + this.mappings.get(i).toString());
			}
	}

	@Test
	public void show () {
		for (String problem : this.problemsFound)
			System.out.println(problem);
	}

}
