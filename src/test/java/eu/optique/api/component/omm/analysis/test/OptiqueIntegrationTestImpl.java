package eu.optique.api.component.omm.analysis.test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.coode.owlapi.rdf.model.RDFGraph;
import org.coode.owlapi.rdf.model.RDFTranslator;
import org.openrdf.model.URI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import uk.ac.manchester.cs.owl.owlapi.OWLDataFactoryImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLOntologyManagerImpl;

import com.fluidops.iwb.AbstractOptiqueIntegrationTest;
import com.fluidops.iwb.datasource.DataSource;
import com.fluidops.iwb.datasource.DataSourceService;
import com.fluidops.iwb.datasource.DataSourceServiceImpl;
import com.fluidops.iwb.datasource.metadata.RelationalMetaDataObject;
import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;
import com.fluidops.iwb.datasource.metadata.impl.RelationalMetaDataObjectImpl;

import eu.optique.api.mapping.R2RMLMappingManagerFactory;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class OptiqueIntegrationTestImpl extends AbstractOptiqueIntegrationTest {

	public List<TestDataSource> getDataSources () {
		return Arrays.asList(TestDataSource.FACTPAGES, TestDataSource.MUSICBRAINZ, TestDataSource.SLEGGE, TestDataSource.SLEGGE_EPI, TestDataSource.STATOIL_FACTPAGES);
	}

	public static URI intializeAndRegisterDataSourceImpl (TestDataSource testDataSource) {
		try {
			return AbstractOptiqueIntegrationTest.intializeAndRegisterDataSource(testDataSource);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public RelationalMetaDataObject getRelationalMetaData (URI dataSourceIdentifier) throws InvalidSchemaSpecificationException {
		DataSourceService dataSourceService = DataSourceServiceImpl.getInstance();
		DataSource ds = dataSourceService.getDataSource(dataSourceIdentifier);
		if (ds == null)
			throw new IllegalArgumentException("Not such DataSource with identifier :" + dataSourceIdentifier + " available.");
		return new RelationalMetaDataObjectImpl(ds.getIdentifier());
	}

	public static Collection<TriplesMap> getR2RMLModel (File file) throws OWLOntologyCreationException, InvalidR2RMLMappingException {
		OWLOntologyManager oom = new OWLOntologyManagerImpl(new OWLDataFactoryImpl());
		OWLOntology ontology = oom.loadOntologyFromOntologyDocument(file);
		RDFGraph model = new RDFTranslator(oom, ontology, false).getGraph();
		return R2RMLMappingManagerFactory.getJenaMappingManager().importMappings(model);
	}

}
