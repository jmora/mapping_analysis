/**
 * 
 */
package eu.optique.api.component.omm.analysis.test;

import java.util.Arrays;

import net.sf.jsqlparser.JSQLParserException;

import org.junit.Test;

import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.sqlapi.OrderSelectItemsVisitor;

/**
 * @author jmora
 * 
 */
public class SQLRepairTest {
	@Test
	public void repairStrings () throws JSQLParserException {
		String q1 = Statics.repairSQLQuery("select column_one as cone, columntwo ctwo from sometable");
		System.out.println(q1);
	}

	@Test
	public void reorderItems () throws JSQLParserException {
		String q1 = OrderSelectItemsVisitor.orderSelectItems("select column_one as cone, columntwo ctwo from sometable", Arrays.asList("ctwo", "cone"));
		System.out.println(q1);
	}
}
