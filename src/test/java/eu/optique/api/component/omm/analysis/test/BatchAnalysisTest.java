package eu.optique.api.component.omm.analysis.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.fluidops.iwb.datasource.metadata.Schema;
import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.convenience.Globals;
import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.interfaces.optique.AnalysisType;
import eu.optique.api.component.omm.analysis.interfaces.optique.BatchAnalyser;
import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class BatchAnalysisTest {

	private static Map<AnalysisType, Iterable<List<HTMLSerializable>>> results;
	private static Set<String> flatResults;

	@BeforeClass
	public static void prepare () throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException, InvalidSchemaSpecificationException,
			OWLOntologyCreationException, InvalidR2RMLMappingException {
		TerminalHelper helper = new TerminalHelper();
		ArrayList<TriplesMap> mappings = new ArrayList<TriplesMap>(helper.getMappings("resources/mappings.ttl"));
		OWLOntology ontology = helper.getOntology("resources/ontology.ttl");
		URIImpl metaDataURI = new URIImpl("http://www.optique-project.eu/mapping-analysis/base/");
		List<Schema> schema = helper.getSchema("resources/databaseSchema.rdf", metaDataURI);
		// File schema = new File("resources/statoilSchema-Simple.schema");
		BatchAnalysisTest.results = BatchAnalyser.analyse(ontology, mappings, schema);
		BatchAnalysisTest.flatResults = BatchAnalysisTest.getFlatResults();
	}

	private static Set<String> getFlatResults () {
		HashSet<String> res = new HashSet<String>();
		for (Entry<AnalysisType, Iterable<List<HTMLSerializable>>> pair : BatchAnalysisTest.results.entrySet())
			for (List<HTMLSerializable> individualResults : pair.getValue())
				for (HTMLSerializable message : individualResults)
					res.add(message.toNamedString());
		return res;
	}

	@Test
	public void show () {
		System.out.println(Statics.mkstring(BatchAnalysisTest.flatResults, "\n", "\nFull batch process results:\n", "\nEnd of full batch process results\n"));
	}

	@Test
	public void testNoFalsePositives () {
		if (Globals.caseinsensitiveness)
			Assert.assertFalse(BatchAnalysisTest.flatResults.size() == 4); // TODO: this number has to be modified accordingly
		else
			Assert.assertFalse(BatchAnalysisTest.flatResults.size() == 4); // TODO: this number has to be modified accordingly
	}

	@Test
	public void testNotGroundedVariables () {
		if (!Globals.caseinsensitiveness)
			this.hasProblem("TriplesMap18", MappingAnalysisRelation.VARIABLE_NOT_GROUNDED, "P, h");
		this.hasProblem("TriplesMap23", MappingAnalysisRelation.VARIABLE_NOT_GROUNDED, "c");
		this.hasProblem("TriplesMap24", MappingAnalysisRelation.VARIABLE_NOT_GROUNDED, "w");
	}

	@Test
	public void testUndefinedVariables () {
		if (!Globals.caseinsensitiveness)
			this.hasProblem("TriplesMap19", MappingAnalysisRelation.UNDEFINED_VARIABLE, "p, m");
		this.hasProblem("TriplesMap17", MappingAnalysisRelation.UNDEFINED_VARIABLE, "e");
		this.hasProblem("TriplesMap22", MappingAnalysisRelation.UNDEFINED_VARIABLE, "h");
	}

	@Test
	public void testAmbiguousVariables () {
		this.hasProblem("TriplesMap17", MappingAnalysisRelation.AMBIGUOUS_VARIABLE, "e");
		this.hasProblem("TriplesMap11", MappingAnalysisRelation.AMBIGUOUS_VARIABLE, "d, c");
	}

	@Test
	public void testSignature () {
		this.hasProblem("TriplesMap25", MappingAnalysisRelation.CLASS_NOT_DEFINED, "http://optique-project.eu/mappings/analysis/test/ontology/RA");
		this.hasProblem("TriplesMap27", MappingAnalysisRelation.PROPERTY_NOT_DEFINED, "http://optique-project.eu/mappings/analysis/test/ontology/asdfg");
	}

	@Test
	public void testHeadInconsistency () {
		this.hasProblem("TriplesMap03", MappingAnalysisRelation.HEAD_INCONSISTENT);
	}

	@Test
	public void testHeadSubsumption () {
		for (String subsuming : "TriplesMap04 TriplesMap05 TriplesMap06 TriplesMap07".split(" "))
			this.hasProblem(subsuming, MappingAnalysisRelation.HEAD_SUBSUMES, "TriplesMap08");
		this.hasProblem("TriplesMap01", MappingAnalysisRelation.HEAD_SUBSUMES, "TriplesMap02");
	}

	private void hasProblem (String mappingAssertion, MappingAnalysisRelation problem) {
		this.hasProblem(mappingAssertion, problem, "");
	}

	private void hasProblem (String mappingAssertion, MappingAnalysisRelation problem, String string) {
		Assert.assertTrue(BatchAnalysisTest.flatResults.contains("{" + mappingAssertion + "} " + problem.message() + " {" + string + "}"));
	}

}
