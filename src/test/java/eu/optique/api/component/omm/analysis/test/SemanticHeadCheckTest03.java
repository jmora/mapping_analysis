package eu.optique.api.component.omm.analysis.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import eu.optique.api.component.omm.analysis.MappingAnalysisOrderedResult;
import eu.optique.api.component.omm.analysis.incremental.IncrementalHeadAnalyser;
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author Marco Ruzzi
 */
public class SemanticHeadCheckTest03 {

	private ArrayList<TriplesMap> mappings;
	private ArrayList<String> problemsFound;
	private OWLOntology ontology;

	@Before
	public void prepare () throws OWLOntologyCreationException, RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, InvalidR2RMLMappingException,
			IOException {
		TerminalHelper helper = new TerminalHelper();
		this.mappings = new ArrayList<TriplesMap>(helper.getMappings("resources/headAnalysisTest/mappings03.ttl"));
		this.ontology = helper.getOntology("resources/headAnalysisTest/ontology03.ttl");
		this.problemsFound = new ArrayList<String>();
		IncrementalHeadAnalyser han = new IncrementalHeadAnalyser(this.mappings, this.ontology);
		for (MappingAnalysisOrderedResult result : han.getResults())
			this.problemsFound.add(result.toResult().toNamedString());
	}

	public void print () {
		for (OWLClass c : this.ontology.getClassesInSignature())
			System.out.println(c);
		for (OWLAxiom a : this.ontology.getAxioms())
			if (!(a instanceof OWLDeclarationAxiom))
				System.out.println(a);
		for (TriplesMap t : this.mappings)
			System.out.println(t);
	}

	@Test
	public void test1 () {
		// print();
		Assert.assertTrue(this.problemsFound.size() == 1);
		System.out.println(this.problemsFound.size());
		for (String problem : this.problemsFound)
			System.out.println(problem);
	}

}
