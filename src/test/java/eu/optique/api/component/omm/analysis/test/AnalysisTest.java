package eu.optique.api.component.omm.analysis.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.openrdf.model.URI;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.fluidops.iwb.AbstractOptiqueIntegrationTest.TestDataSource;

import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * Unit test for simple App.
 */
public class AnalysisTest extends TestCase {
	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 * @throws InvalidR2RMLMappingException
	 * @throws OWLOntologyCreationException
	 */
	public AnalysisTest (String testName) throws OWLOntologyCreationException, InvalidR2RMLMappingException {
		super(testName);
		OptiqueIntegrationTestImpl provider = new OptiqueIntegrationTestImpl();
		Collection<TriplesMap> r2rmlMaps = OptiqueIntegrationTestImpl.getR2RMLModel(new File("mappings.ttl"));
		ArrayList<URI> uris = new ArrayList<URI>();
		for (TestDataSource ds: provider.getDataSources())
			uris.add(OptiqueIntegrationTestImpl.intializeAndRegisterDataSourceImpl(ds));
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite () {
		return new TestSuite(AnalysisTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testApp () {
		Assert.assertTrue(true);
	}
}
