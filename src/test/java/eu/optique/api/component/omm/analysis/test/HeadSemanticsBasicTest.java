package eu.optique.api.component.omm.analysis.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import eu.optique.api.component.omm.analysis.MappingAnalysisOrderedResult;
import eu.optique.api.component.omm.analysis.incremental.IncrementalHeadAnalyser;
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

public class HeadSemanticsBasicTest {
	private ArrayList<TriplesMap> mappings;
	private ArrayList<String> problemsFound;

	@Before
	public void prepare () throws OWLOntologyCreationException, RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, InvalidR2RMLMappingException,
			IOException {
		TerminalHelper helper = new TerminalHelper();
		this.mappings = new ArrayList<TriplesMap>(helper.getMappings("resources/mappings.ttl"));
		OWLOntology ontology = helper.getOntology("resources/ontology.ttl");
		this.problemsFound = new ArrayList<String>();
		IncrementalHeadAnalyser han = new IncrementalHeadAnalyser(this.mappings, ontology);
		for (MappingAnalysisOrderedResult result : han.getResults())
			this.problemsFound.add(result.toResult().toNamedString());
	}

	@Test
	public void show () {
		System.out.println(this.problemsFound.size());
		for (String problem : this.problemsFound)
			System.out.println(problem);
	}

}
