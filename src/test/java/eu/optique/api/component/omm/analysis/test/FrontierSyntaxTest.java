package eu.optique.api.component.omm.analysis.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.jsqlparser.JSQLParserException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.fluidops.iwb.datasource.metadata.Schema;
import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.convenience.Globals;
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedSingleResult;
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper;
import eu.optique.api.component.omm.analysis.sqlapi.SQLPartialAPI;
import eu.optique.api.component.omm.analysis.syntax.BodyChecker;
import eu.optique.api.component.omm.analysis.syntax.FrontierChecker;
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class FrontierSyntaxTest {

	private ArrayList<TriplesMap> mappings;
	private HashMap<String, Set<String>> signature;
	private ArrayList<String> problemsFound;

	@Before
	public void prepare () throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException, InvalidSchemaSpecificationException,
			OWLOntologyCreationException, InvalidR2RMLMappingException {
		TerminalHelper helper = new TerminalHelper();
		this.mappings = new ArrayList<TriplesMap>(helper.getMappings("resources/mappings.ttl"));
		URI metaDataURI = new URIImpl("http://www.optique-project.eu/mapping-analysis/base/");
		List<Schema> schema = helper.getSchema("resources/databaseSchema.rdf", metaDataURI);
		this.signature = SyntacticAnalyserImpl.getSignature(schema);
		SQLPartialAPI spapi;
		BodyChecker bodyChecker;
		HashSet<Integer> mapping;
		this.problemsFound = new ArrayList<String>();
		for (int i = 0; i < this.mappings.size(); i++)
			try {
				spapi = new SQLPartialAPI(this.mappings.get(i));
				mapping = new HashSet<Integer>();
				bodyChecker = new BodyChecker(spapi, this.signature, mapping, this.mappings);
				for (MappingAnalysisOrderedSingleResult result : FrontierChecker.check(this.mappings, i, this.signature, bodyChecker))
					this.problemsFound.add(result.toNamedString());
			} catch (JSQLParserException e) {
				System.out.println("Parser error for mapping: " + this.mappings.get(i).toString());
			}
	}

	public void show () {
		for (String problem : this.problemsFound)
			System.out.println(problem);
	}

	@Test
	public void testNotGroundedVariables () {
		if (Globals.caseinsensitiveness)
			Assert.assertTrue(this.problemsFound.size() == 2);
		else {
			Assert.assertTrue(this.problemsFound.size() == 3);
			Assert.assertTrue(this.problemsFound.contains("{TriplesMap18} " + MappingAnalysisRelation.VARIABLE_NOT_GROUNDED.message() + "{P, h}"));
		}
		Assert.assertTrue(this.problemsFound.contains("{TriplesMap23} " + MappingAnalysisRelation.VARIABLE_NOT_GROUNDED.message() + "{c}"));
		Assert.assertTrue(this.problemsFound.contains("{TriplesMap24} " + MappingAnalysisRelation.VARIABLE_NOT_GROUNDED.message() + "{w}"));
	}

}
