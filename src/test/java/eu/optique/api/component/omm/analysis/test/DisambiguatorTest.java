/**
 * 
 */
package eu.optique.api.component.omm.analysis.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;

import eu.optique.api.component.omm.analysis.incremental.Disambiguator;
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class DisambiguatorTest {
	private ArrayList<TriplesMap> mappings;
	private Disambiguator disambiguator;

	@Before
	public void prepare () throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException, InvalidSchemaSpecificationException,
			OWLOntologyCreationException, InvalidR2RMLMappingException {
		TerminalHelper helper = new TerminalHelper();
		this.mappings = new ArrayList<TriplesMap>(helper.getMappings("resources/mappings.ttl"));
		OWLOntology ontology = helper.getOntology("resources/ontology.ttl");
		this.disambiguator = new Disambiguator(ontology);
	}

	@Test
	public void TestClasses () {
		ArrayList<String> superficialCheck = new ArrayList<String>();
		for (TriplesMap triplesMap : this.mappings)
			for (OWLClass owlClass : this.disambiguator.getAllClasses(triplesMap))
				superficialCheck.add(owlClass.getIRI().toString());
		Assert.assertTrue(superficialCheck.contains("http://optique-project.eu/mappings/analysis/test/ontology/PH"));
		Assert.assertFalse(superficialCheck.contains("http://optique-project.eu/mappings/analysis/test/ontology/RA"));
		Assert.assertTrue(superficialCheck.size() == 22);
	}
}
