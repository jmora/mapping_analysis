package eu.optique.api.component.omm.analysis.test;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.JSQLParserException;

import org.junit.Assert;
import org.junit.Test;

import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.sqlapi.FromTableCaseClasses.Source;
import eu.optique.api.component.omm.analysis.sqlapi.FromTableCaseClasses.SourceClass;
import eu.optique.api.component.omm.analysis.sqlapi.SQLPartialAPI;

/**
 * @author jmora
 * 
 */
public class SQLQueryTest {

	/* SQL PARSING */
	@Test
	public void headVariablesOK () throws JSQLParserException {
		SQLPartialAPI spapi = new SQLPartialAPI("select a, b, c from table1 join table2 on a = b where c < 20");
		List<String> headcols = spapi.getFrontierVariables();
		String res = Statics.mkstring(headcols);
		Assert.assertTrue(res.equals("[ a, b, c ]"));
	}

	@Test
	public void headVariablesOK2 () throws JSQLParserException {
		SQLPartialAPI spapi = new SQLPartialAPI("select * from table1 join (select a as d, h as o from table2) on a = b where c < 20");
		List<String> headcols = spapi.getFrontierVariables();
		Assert.assertTrue(Statics.mkstring(headcols).equals("[ * ]"));
	}

	@Test
	public void headVariablesOK3 () throws JSQLParserException {
		SQLPartialAPI spapi = new SQLPartialAPI("select a as e, b as f, c as g from table1 inner join table2 on a = b where c < 20");
		List<String> headcols = spapi.getFrontierVariables();
		String res = Statics.mkstring(headcols);
		Assert.assertTrue(res.equals("[ e, f, g ]"));
		headcols = spapi.getInternalVariables();
		res = Statics.mkstring(headcols);
		Assert.assertTrue(res.equals("[ a, b, c ]"));
	}

	@Test
	public void headVariablesOK4 () throws JSQLParserException {
		SQLPartialAPI spapi = new SQLPartialAPI("select avg(a) as e from table1");
		List<String> headcols = spapi.getFrontierVariables();
		String res = Statics.mkstring(headcols);
		Assert.assertTrue(res.equals("[ e ]"));
		Assert.assertTrue(Statics.mkstring(spapi.getInternalVariables()).equals("[ a ]"));
	}

	@Test
	public void headTablesOK1 () throws JSQLParserException {
		SQLPartialAPI spapi = new SQLPartialAPI("select t1.a, t2.b, c from table1 as t1 inner join table2 as t2 on t1.a = t2.b where t2.c < 20");
		List<Source> tables = spapi.getBodyTables();
		Assert.assertTrue(Statics.mkstring(spapi.getFrontierVariables()).equals("[ t1.a, t2.b, c ]"));
		Assert.assertTrue(Statics.mkstring(spapi.getInternalVariables()).equals("[ t1.a, t2.b, c ]"));
		List<String> tres = new ArrayList<String>();
		List<String> tres2 = new ArrayList<String>();
		for (Source s : tables)
			if (s.getSourceClass() == SourceClass.Table) {
				tres.add(s.getAlias());
				tres2.add(s.getTableName());
			}
		Assert.assertTrue(Statics.mkstring(tres).equals("[ t1, t2 ]"));
		Assert.assertTrue(Statics.mkstring(tres2).equals("[ table1, table2 ]"));
	}

	@Test
	public void headTablesOK2 () throws JSQLParserException {
		SQLPartialAPI spapi = new SQLPartialAPI("select t1.a, t2.b, c from table1 as t1 inner join ( select * from table2 ) as t2 on t1.a = t2.b where t2.c < 20");
		List<Source> tables = spapi.getBodyTables();
		Assert.assertTrue(Statics.mkstring(spapi.getFrontierVariables()).equals("[ t1.a, t2.b, c ]"));
		Assert.assertTrue(Statics.mkstring(spapi.getInternalVariables()).equals("[ t1.a, t2.b, c ]"));
		List<String> tres = new ArrayList<String>();
		List<String> tres2 = new ArrayList<String>();
		for (Source s : tables) {
			if (s.getSourceClass() == SourceClass.Table) {
				tres.add(s.getAlias());
				tres2.add(s.getTableName());
			}
			if (s.getSourceClass() == SourceClass.Subselect) {
				tres.add(s.getAlias());
				tres2.add(s.getSubselect().toString());
			}
		}
		Assert.assertTrue(Statics.mkstring(tres).equals("[ t1, t2 ]"));
		Assert.assertTrue(Statics.mkstring(tres2).equals("[ table1, SELECT * FROM table2 ]"));
	}

	@Test
	public void FrontierVariables () throws JSQLParserException {
		List<String> fvars = new SQLPartialAPI(
				"select 'm' as unit, SUM(TOP_DEPTH-BOTTOM_DEPTH) as LENGTH , WELLBORE_ID as WELLBORE_ID from SLEGGE_EPI.WELLBORE_INTV, SLEGGE_EPI.UNCERTAINTY_CLS UNCERTAINTY_CLASS, "
						+ "SLEGGE_EPI.POS_UNCR_CLSN depth_clsn where UNCERTAINTY_CLASS.CLSN_CLS_NAME = 'drillers depth' AND UNCERTAINTY_CLASS.CLASSIFICATION_SYS = 'depth type uncertainty' "
						+ "AND depth_clsn.EMODEL_OBJECT_S = WELLBORE_INTV.WELLBORE_INTV_S AND depth_clsn.UNCERTAINTY_CLS_S = UNCERTAINTY_CLASS.UNCERTAINTY_CLS_S group by WELLBORE_ID")
				.getFrontierVariables();
		Assert.assertTrue(Statics.mkstring(fvars).equals("[ unit, LENGTH, WELLBORE_ID ]"));
	}

}
