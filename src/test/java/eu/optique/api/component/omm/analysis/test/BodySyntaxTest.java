package eu.optique.api.component.omm.analysis.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import net.sf.jsqlparser.JSQLParserException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.fluidops.iwb.datasource.metadata.Schema;
import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.convenience.Globals;
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedSingleResult;
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper;
import eu.optique.api.component.omm.analysis.sqlapi.SQLPartialAPI;
import eu.optique.api.component.omm.analysis.syntax.BodyChecker;
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class BodySyntaxTest {

	private ArrayList<TriplesMap> mappings;
	private HashMap<String, Set<String>> signature;
	private ArrayList<String> problemsFound;

	@Before
	public void prepare () throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException, InvalidSchemaSpecificationException,
			OWLOntologyCreationException, InvalidR2RMLMappingException {
		TerminalHelper helper = new TerminalHelper();
		this.mappings = new ArrayList<TriplesMap>(helper.getMappings("resources/mappings.ttl"));
		URI metaDataURI = new URIImpl("http://www.optique-project.eu/mapping-analysis/base/");
		List<Schema> schema = helper.getSchema("resources/databaseSchema.rdf", metaDataURI);
		this.signature = SyntacticAnalyserImpl.getSignature(schema);
		SQLPartialAPI spapi;
		this.problemsFound = new ArrayList<String>();
		for (int i = 0; i < this.mappings.size(); i++)
			try {
				spapi = new SQLPartialAPI(this.mappings.get(i));
				for (MappingAnalysisOrderedSingleResult result : BodyChecker.check(this.mappings, i, this.signature, spapi))
					this.problemsFound.add(result.toNamedString());
			} catch (JSQLParserException e) {
				System.out.println("Parser error for mapping: " + this.mappings.get(i).toString());
			}
	}

	public void show () {
		SQLPartialAPI spapi;
		for (int i = 0; i < this.mappings.size(); i++)
			try {
				spapi = new SQLPartialAPI(this.mappings.get(i));
				System.out.println("Checking " + this.mappings.get(i).getResource(Globals.sesameResource));
				for (MappingAnalysisOrderedSingleResult result : BodyChecker.check(this.mappings, i, this.signature, spapi))
					System.out.println(result.toNamedString());
			} catch (JSQLParserException e) {
				System.out.println("Parser error for mapping: " + this.mappings.get(i).toString());
			}
	}

	@Test
	public void testUndefinedVariables () {
		if (Globals.caseinsensitiveness)
			Assert.assertTrue(this.problemsFound.size() == 4);
		else {
			Assert.assertTrue(this.problemsFound.size() == 5);
			Assert.assertTrue(this.problemsFound.contains("{TriplesMap19} " + MappingAnalysisRelation.UNDEFINED_VARIABLE.message() + "{p, m}"));
		}
		Assert.assertTrue(this.problemsFound.contains("{TriplesMap17} " + MappingAnalysisRelation.UNDEFINED_VARIABLE.message() + "{e}"));
		Assert.assertTrue(this.problemsFound.contains("{TriplesMap22} " + MappingAnalysisRelation.UNDEFINED_VARIABLE.message() + "{h}"));
	}

	@Test
	public void testAmbiguousVariables () {
		if (Globals.caseinsensitiveness)
			Assert.assertTrue(this.problemsFound.size() == 4);
		else
			Assert.assertTrue(this.problemsFound.size() == 5);
		Assert.assertTrue(this.problemsFound.contains("{TriplesMap11} " + MappingAnalysisRelation.AMBIGUOUS_VARIABLE.message() + "{d, c}"));
		Assert.assertTrue(this.problemsFound.contains("{TriplesMap17} " + MappingAnalysisRelation.AMBIGUOUS_VARIABLE.message() + "{e}"));
	}

}
