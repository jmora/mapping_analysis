package eu.optique.api.mapping.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.semanticweb.owlapi.model.IRI;

import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.mapping.SubjectMap;
import eu.optique.api.mapping.TermMap;
import eu.optique.api.mapping.TermMap.TermMapType;

/**
 * @author jmora
 * 
 */
public class TemplateAccessor implements Comparable<TemplateAccessor> {

	private TemplateImpl template;
	private Log log = LogFactory.getLog(TemplateAccessor.class);
	private TermMap termMap;

	public TemplateAccessor(TermMap termMap) {
		this.termMap = termMap;
		this.template = this.obtainTemplate(termMap);
	}

	private TemplateImpl obtainTemplate (TermMap termMap) {
		if (termMap.getTermMapType() == TermMapType.TEMPLATE_VALUED)
			return new TemplateImpl(termMap.getTemplateString());
		return null;
	}

	public TermMap getTermMap () {
		return this.termMap;
	}

	public String getAsString () {
		return Statics.mkstring(this.template.segList, "$$", "", "");
	}

	public boolean equivalent (TermMap that) {
		if (this.termMap.getTermMapType() != that.getTermMapType())
			return false;
		if (that.getTermMapType() == TermMapType.COLUMN_VALUED)
			return true;
		if (that.getTermMapType() == TermMapType.CONSTANT_VALUED)
			return this.termMap.getConstant().equals(that.getConstant());
		TemplateImpl that_template = new TemplateImpl(that.getTemplateString());
		if (this.template.segList.size() != that_template.segList.size() || that_template.colList.size() != this.template.colList.size())
			return false;
		for (int i = 0; i < this.template.segList.size(); i++)
			if (!this.template.segList.get(i).equals(that_template.segList.get(i)))
				return false;
		return true;
	}

	public boolean equal (TermMap termMap2) {
		if (this.termMap.getTermMapType() != termMap2.getTermMapType())
			return false;
		switch (termMap2.getTermMapType()) {
		case COLUMN_VALUED:
			return termMap2.getColumn().equals(this.termMap.getColumn());
		case CONSTANT_VALUED:
			return this.termMap.getConstant().equals(termMap2.getConstant());
		case TEMPLATE_VALUED:
			return this.equalTemplates(this.obtainTemplate(this.termMap), this.obtainTemplate(termMap2));
		default:
			break;
		}
		return false;
	}

	private boolean equalTemplates (TemplateImpl template1, TemplateImpl template2) {
		if (template1.segList.size() != template2.segList.size() || template2.colList.size() != template1.colList.size())
			return false;
		for (int i = 0; i < template1.segList.size(); i++)
			if (template1.segList.get(i) != template2.segList.get(i))
				return false;
		for (int i = 0; i < template1.colList.size(); i++)
			if (template1.colList.get(i) != template2.colList.get(i))
				return false;
		return true;
	}

	public ArrayList<String> getVariables () {
		switch (this.termMap.getTermMapType()) {
		case COLUMN_VALUED:
			return new ArrayList<String>(Arrays.asList(this.termMap.getColumn()));
		case CONSTANT_VALUED:
			return new ArrayList<String>();
		case TEMPLATE_VALUED:
			return new ArrayList<String>(this.template.colList);
		default:
			return new ArrayList<String>();
		}
	}

	@Override
	public int compareTo (TemplateAccessor that) {
		int r = this.termMap.getTermMapType().compareTo(that.termMap.getTermMapType());
		if (r != 0)
			return r;
		switch (this.termMap.getTermMapType()) {
		case COLUMN_VALUED:
			return this.termMap.getColumn().compareTo(that.termMap.getColumn());
		case CONSTANT_VALUED:
			return this.termMap.getConstant().compareTo(that.termMap.getConstant());
		case TEMPLATE_VALUED:
			return this.termMap.getTemplateString().compareTo(that.termMap.getTemplateString());
		default:
			break;
		}
		this.log.warn("A comparison between terms went wrong. These:\n" + this.toString() + "\nand\n" + that.toString() + "\n\n");
		return 0;
	}

	public IRI getAsIRIsh () {
		switch (this.termMap.getTermMapType()) {
		case COLUMN_VALUED:
			return IRI.create(this.termMap.getColumn());
		case CONSTANT_VALUED:
			return IRI.create(this.termMap.getConstant());
		case TEMPLATE_VALUED:
			return IRI.create(this.termMap.getTemplateString());
		default:
			break;
		}
		return null;
	}

	public List<List<String>> renames (SubjectMap subjectMap) {
		// TODO Auto-generated method stub
		return null;
	}
}
