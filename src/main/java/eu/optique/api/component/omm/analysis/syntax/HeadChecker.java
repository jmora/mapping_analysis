package eu.optique.api.component.omm.analysis.syntax;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.impl.URIImpl;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.convenience.Globals;
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedSingleResult;
import eu.optique.api.mapping.ObjectMap;
import eu.optique.api.mapping.PredicateMap;
import eu.optique.api.mapping.PredicateObjectMap;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.TemplateAccessor;

/**
 * @author jmora
 * 
 */
public class HeadChecker {

	public static ArrayList<MappingAnalysisOrderedSingleResult> check (ArrayList<TriplesMap> mappings, int index, HashMap<String, Set<String>> signature) {
		ArrayList<MappingAnalysisOrderedSingleResult> result = new ArrayList<MappingAnalysisOrderedSingleResult>();
		HashSet<Integer> thisMapping = new HashSet<Integer>();
		thisMapping.add(index);
		TriplesMap mapping = mappings.get(index);
		Set<String> missingDefs = HeadChecker.getConcepts(mapping, signature);
		if (!missingDefs.isEmpty())
			result.add(new MappingAnalysisOrderedSingleResult(thisMapping, MappingAnalysisRelation.CLASS_NOT_DEFINED, missingDefs, mappings));
		missingDefs = HeadChecker.getObjectProperties(mapping, signature);
		if (!missingDefs.isEmpty())
			result.add(new MappingAnalysisOrderedSingleResult(thisMapping, MappingAnalysisRelation.PROPERTY_NOT_DEFINED, missingDefs, mappings));
		return result;
	}

	private static Set<String> getObjectProperties (TriplesMap mapping, HashMap<String, Set<String>> signature) {
		Set<String> res = new HashSet<String>();
		for (PredicateObjectMap map : mapping.getPredicateObjectMaps())
			for (PredicateMap pred : map.getPredicateMaps()) {
				String property = HeadChecker.getString(pred);
				if ("@skip this one@".equals(property))
					continue;
				if (!signature.get(SyntacticAnalyserImpl.ROLE).contains(property) && !signature.get(SyntacticAnalyserImpl.ATTRIBUTE).contains(property))
					res.add(property);
			}
		return res;
	}

	public static String getString (PredicateMap pred) {
		String res;
		if (pred.getConstant() != null)
			return pred.getConstant();
		try {
			res = pred.getResource(Globals.sesameResource).toString();
		} catch (Exception e) {
			res = pred.getResource(Globals.sesameAnonymousResource).toString();
			res = "@skip this one@";
		}
		return res;
	}

	private static Set<String> getConcepts (TriplesMap mapping, HashMap<String, Set<String>> signature) {
		HashSet<String> res = new HashSet<String>();
		List<URIImpl> concepts = mapping.getSubjectMap().getClasses(Globals.sesameResource);
		for (URIImpl concept : concepts)
			if (!signature.get(SyntacticAnalyserImpl.CLASS).contains(concept.toString()))
				res.add(concept.toString());
		return res;
	}

	public static Set<String> getHeadVariables (TriplesMap mapping) {
		HashSet<String> res = new HashSet<String>();
		res.addAll(new TemplateAccessor(mapping.getSubjectMap()).getVariables());
		for (PredicateObjectMap po : mapping.getPredicateObjectMaps()) {
			for (PredicateMap p : po.getPredicateMaps())
				res.addAll(new TemplateAccessor(p).getVariables());
			for (ObjectMap o : po.getObjectMaps())
				res.addAll(new TemplateAccessor(o).getVariables());
		}
		return res;
	}

}
