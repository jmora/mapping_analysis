/**
 * 
 */
package eu.optique.api.component.omm.analysis.interfaces.optique;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.semanticweb.owlapi.model.OWLOntology;

import com.fluidops.iwb.datasource.metadata.Schema;

import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.impl.BodyChecker;
import eu.optique.api.component.omm.analysis.incremental.Disambiguator;
import eu.optique.api.component.omm.analysis.incremental.HermitRoleIntersectionConsistencyChecker;
import eu.optique.api.component.omm.analysis.incremental.HermitRoleIntersectionSubsumptionChecker;
import eu.optique.api.component.omm.analysis.incremental.OntologyAndReasonerWrap;
import eu.optique.api.component.omm.analysis.incremental.RoleIntersectionConjunctor;
import eu.optique.api.component.omm.analysis.lazy.ConsistencyResultsIterable;
import eu.optique.api.component.omm.analysis.lazy.SubsumptionResultsIterable;
import eu.optique.api.component.omm.analysis.lazy.SyntacticResultsIterable;
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class BatchAnalyser {

	public static Map<AnalysisType, Iterable<List<HTMLSerializable>>> analyse (OWLOntology ontology, Collection<TriplesMap> mappings, List<Schema> schemata) {
		return BatchAnalyser.analyse(ontology, mappings, schemata, AnalysisType.Subsumption);
	}

	public static Map<AnalysisType, Iterable<List<HTMLSerializable>>> analyse (OWLOntology ontology, Collection<TriplesMap> mappings, List<Schema> schemata, AnalysisType analysisType) {
		BodyChecker bodyChecker = new BodyChecker(schemata);
		SyntacticAnalyserImpl syntacticAnalyser = new SyntacticAnalyserImpl(ontology, schemata);
		return BatchAnalyser.analyse(mappings, bodyChecker, ontology, syntacticAnalyser);
	}

	public static Map<AnalysisType, Iterable<List<HTMLSerializable>>> analyse (OWLOntology ontology, Collection<TriplesMap> mappings, File schemata) {
		return BatchAnalyser.analyse(ontology, mappings, schemata, AnalysisType.Subsumption);
	}

	public static Map<AnalysisType, Iterable<List<HTMLSerializable>>> analyse (OWLOntology ontology, Collection<TriplesMap> mappings, File schemata, AnalysisType analysisType) {
		BodyChecker bodyChecker = new BodyChecker(schemata);
		SyntacticAnalyserImpl syntacticAnalyser = new SyntacticAnalyserImpl(ontology, schemata);
		return BatchAnalyser.analyse(mappings, bodyChecker, ontology, syntacticAnalyser);
	}

	private static Map<AnalysisType, Iterable<List<HTMLSerializable>>> analyse (Collection<TriplesMap> mappings, BodyChecker bodyChecker, OWLOntology ontology, SyntacticAnalyserImpl syntacticAnalyser) {
		OWLOntology ontologyCopy = Statics.copyOntology(ontology);
		Map<AnalysisType, Iterable<List<HTMLSerializable>>> res = new HashMap<AnalysisType, Iterable<List<HTMLSerializable>>>();
		Disambiguator disambiguator = new Disambiguator(ontologyCopy);
		OntologyAndReasonerWrap ontologyAndReasonerWrap = new OntologyAndReasonerWrap(ontologyCopy);
		RoleIntersectionConjunctor iconjunctor = new RoleIntersectionConjunctor(ontologyAndReasonerWrap, disambiguator);
		HermitRoleIntersectionConsistencyChecker headConsistencyChecker = new HermitRoleIntersectionConsistencyChecker(iconjunctor, ontologyAndReasonerWrap);
		HermitRoleIntersectionSubsumptionChecker headSubsumptionChecker = new HermitRoleIntersectionSubsumptionChecker(iconjunctor, ontologyAndReasonerWrap);
		headSubsumptionChecker = new HermitRoleIntersectionSubsumptionChecker(iconjunctor, ontologyAndReasonerWrap);
		ArrayList<SingleAnalyser> analysers = new ArrayList<SingleAnalyser>();
		for (TriplesMap triplesMap : mappings)
			analysers.add(new SingleAnalyser(syntacticAnalyser, headConsistencyChecker, headSubsumptionChecker, bodyChecker, triplesMap));
		res.put(AnalysisType.Syntactic, new SyntacticResultsIterable(analysers));
		res.put(AnalysisType.Consistency, new ConsistencyResultsIterable(analysers));
		res.put(AnalysisType.Subsumption, new SubsumptionResultsIterable(analysers));
		return res;
	}
}
