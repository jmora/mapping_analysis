package eu.optique.api.component.omm.analysis;

/**
 * @author jmora
 * 
 */
public enum MappingAnalysisRelation {
	BODY_SUBSUMES("mappings body-subsume the mappings: "), // The mappings on the right hand side are body-subsumed by the mappings on the left hand side
	HEAD_SUBSUMES("mappings head-subsume the mappings: "), // The mappings on the right hand side are head-subsumed by the mappings on the left hand side
	HEAD_INCONSISTENT("mapping head is inconsistent"), // It's inconsistent, inconsistent I said (I didn't want to write this, Marco forced me, sorry)
	MAPPING_SUBSUMES("mappings subsume the mappings: "), // The mappings on the right hand side are subsumed by the mappings on the left hand side
	HEAD_DISJOINT("mappings are head-disjoint with mappings: "), // The mappings on the right hand side are head-disjoint with the mappings on the left hand side
	BODY_DISJOINT("mappings are body-disjoint with mappings: "), // The mappings on the right hand side are body-disjoint with the mappings on the left hand side
	CONFLICTS("mapping definitions conflict with: "), // The mappings on the right hand side produce a contradiction with the mappings on the left hand side
	VARIABLE_NOT_GROUNDED("mapping head uses variables not defined in the mapping body: "), // The mapping contains not grounded (aka unsafe) variables (used in the head and not declared in the body)
	CLASS_NOT_DEFINED("mapping head uses classes not found in the ontology signature: "), // The mapping uses classes that are not defined in the signature of the ontology
	PROPERTY_NOT_DEFINED("mapping head uses properties not found in the ontology signature: "), // The mapping uses properties that are not defined in the signature of the ontology
	TABLE_NOT_FOUND("SQL query references tables not found in the database signature: "), // The mapping uses tables that are not defined in the signature of the database schema
	COLUMN_NOT_FOUND("SQL query references columns not found in the database signature: "), // The mapping uses columns that are not defined in the signature of the database schema
	PARSER_ERROR("SQL query cannot be parsed, error: "), // The SQL query for this mapping could not be parsed, producing the following error
	AMBIGUOUS_ALIAS("SQL query contains ambiguos aliases: "), // The SQL query contains ambiguous aliases (alias clash)
	UNDEFINED_VARIABLE("SQL query uses undefined column identifiers: "), // The SQL query uses undefined variables (not found as columns in the tables nor aliases in the subqueries)
	AMBIGUOUS_VARIABLE("SQL uses ambiguous column identifiers: "), // The SQL query refers to variables that are defined in several tables or subqueries (hence ambiguous)
	UNDEFINED_TABLE("SQL query uses tables not found in the signature: "), // The mapping uses tables that are not defined in the signature of the database schema //TODO: delete this one
	BODY_INCONSISTENT("mapping body is inconsistent: "), //
	MAPPING_INCONSISTENT("The mapping is inconsistent due to head or body inconsistencies "), //
	EXCEPTION_BODY_CONSISTENCY("There was an exception checking body consistency: "), //
	EXCEPTION_BODY_SUBSUMPTION("There was an exception checking body subsumption: ");

	private final String message;

	public String message () {
		return this.message;
	}

	MappingAnalysisRelation(String message) {
		this.message = message;
	}
}
