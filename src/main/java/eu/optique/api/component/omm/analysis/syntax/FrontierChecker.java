package eu.optique.api.component.omm.analysis.syntax;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedSingleResult;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class FrontierChecker {

	public static ArrayList<MappingAnalysisOrderedSingleResult> check (ArrayList<TriplesMap> mappings, int index, HashMap<String, Set<String>> signature, BodyChecker bodyChecker) {
		ArrayList<MappingAnalysisOrderedSingleResult> result = new ArrayList<MappingAnalysisOrderedSingleResult>();
		TriplesMap triplesMap = mappings.get(index);
		List<String> bodyvars = bodyChecker.getFrontierVariables();
		HashSet<String> badvars = new HashSet<String>();
		Set<String> headvars = HeadChecker.getHeadVariables(triplesMap);
		for (String var : headvars)
			if (!bodyvars.contains(Statics.correctCase(var)))
				badvars.add(var);
		if (!badvars.isEmpty()) {
			HashSet<Integer> thisMapping = new HashSet<Integer>();
			thisMapping.add(index);
			result.add(new MappingAnalysisOrderedSingleResult(thisMapping, MappingAnalysisRelation.VARIABLE_NOT_GROUNDED, badvars, mappings));
		}
		return result;
	}

}
