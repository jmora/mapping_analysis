/**
 * 
 */
package eu.optique.api.component.omm.analysis.incremental;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;

import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.mapping.ObjectMap;
import eu.optique.api.mapping.PredicateMap;
import eu.optique.api.mapping.PredicateObjectMap;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.TemplateAccessor;

/**
 * @author jmora
 * 
 */
public class RoleIntersectionConjunctor {

	private Disambiguator disambiguator;
	private OntologyAndReasonerWrap ontologyAndReasonerWrap;
	private HashMap<String, OWLClass> memoization;
	private OWLDataFactory factory;
	private Log log = LogFactory.getLog(RoleIntersectionConjunctor.class);
	private int propertyID = 1;
	private int classID = 1;

	public RoleIntersectionConjunctor(OntologyAndReasonerWrap ontologyAndReasonerWrap, Disambiguator disambiguator) {
		this.ontologyAndReasonerWrap = ontologyAndReasonerWrap;
		this.disambiguator = disambiguator;
		this.memoization = new HashMap<String, OWLClass>();
		this.factory = OWLManager.createOWLOntologyManager().getOWLDataFactory();
	}

	public OWLClass getConjunction (TriplesMap triplesMap) {
		// Memoization
		String triplesMapURI = triplesMap.getResource(Object.class).toString();
		if (this.memoization.containsKey(triplesMapURI))
			return this.memoization.get(triplesMapURI);
		Set<OWLClassExpression> intersection = new HashSet<OWLClassExpression>();
		intersection.addAll(this.disambiguator.getAllClasses(triplesMap));
		intersection.addAll(this.getIntersectedProperties(triplesMap));
		OWLClass resultingClassName = this.equivalentIntersection(triplesMap, intersection);
		this.memoization.put(triplesMapURI, resultingClassName);
		return resultingClassName;
	}

	private Collection<? extends OWLClassExpression> getIntersectedProperties (TriplesMap triplesMap) {
		ArrayList<OWLClassExpression> res = new ArrayList<OWLClassExpression>();
		TreeMap<TemplateAccessor, TreeSet<TemplateAccessor>> objectsWithJoins = new TreeMap<TemplateAccessor, TreeSet<TemplateAccessor>>();
		for (PredicateObjectMap predicateObjectMap : triplesMap.getPredicateObjectMaps()) {
			TreeSet<TemplateAccessor> predicatesInThisMap = new TreeSet<TemplateAccessor>();
			for (PredicateMap predicateMap : predicateObjectMap.getPredicateMaps()) {
				OWLEntity pm = this.disambiguator.getProperty(predicateMap);
				if (pm == null)
					continue;
				res.add(this.getOWLClassExpression(pm));
				predicatesInThisMap.add(new TemplateAccessor(predicateMap));
			}
			for (ObjectMap objectMap : predicateObjectMap.getObjectMaps()) {
				TemplateAccessor currentObject = new TemplateAccessor(objectMap);
				if (!objectsWithJoins.containsKey(currentObject))
					objectsWithJoins.put(currentObject, new TreeSet<TemplateAccessor>());
				objectsWithJoins.get(currentObject).addAll(predicatesInThisMap);
			}
		}
		for (Entry<TemplateAccessor, TreeSet<TemplateAccessor>> pair : objectsWithJoins.entrySet())
			if (pair.getValue().size() > 1)
				res.add(this.axiomsForJoin(pair.getValue(), triplesMap));
		return res;
	}

	private OWLClassExpression axiomsForJoin (Set<TemplateAccessor> value, TriplesMap triplesMap) {
		OWLObjectProperty fancyNewRole = this.getFancyNewRole(triplesMap);
		OWLClassExpression straight = this.factory.getOWLObjectSomeValuesFrom(fancyNewRole, this.factory.getOWLThing());
		OWLClassExpression inverse = this.factory.getOWLObjectSomeValuesFrom(this.factory.getOWLObjectInverseOf(fancyNewRole), this.factory.getOWLThing());
		HashSet<OWLClassExpression> straights = new HashSet<OWLClassExpression>();
		HashSet<OWLClassExpression> inverses = new HashSet<OWLClassExpression>();
		for (TemplateAccessor role : value) {
			OWLEntity property = this.disambiguator.getProperty(role.getTermMap());
			straights.add(this.getOWLClassExpression(property));
			inverses.addAll(this.getInverseOWLClassExpressionMaybe(property));
		}
		if (inverses.isEmpty())
			straight = this.factory.getOWLDataSomeValuesFrom(this.getFancyNewAttribute(triplesMap), this.factory.getTopDatatype());
		else
			this.equivalentIntersection(inverse, inverses);
		this.equivalentIntersection(straight, straights);
		return straight;
	}

	private OWLObjectProperty getFancyNewRole (TriplesMap triplesMap) {
		String fancyNewIRI = Statics.sesameResourceToIRI(triplesMap).toString() + "_fragmented_conjunction_" + this.propertyID++;
		return this.factory.getOWLObjectProperty(IRI.create(fancyNewIRI));
	}

	private OWLDataProperty getFancyNewAttribute (TriplesMap triplesMap) {
		String fancyNewIRI = Statics.sesameResourceToIRI(triplesMap).toString() + "_fragmented_conjunction_" + this.propertyID++;
		return this.factory.getOWLDataProperty(IRI.create(fancyNewIRI));
	}

	private OWLClass equivalentIntersection (TriplesMap triplesMap, Set<? extends OWLClassExpression> intersection) {
		OWLClassExpression intersectionClass;
		if (intersection.isEmpty())
			return this.factory.getOWLThing();
		if (intersection.size() == 1)
			intersectionClass = intersection.iterator().next();
		else
			intersectionClass = this.factory.getOWLObjectIntersectionOf(intersection);
		return this.getEquivalentClass(triplesMap, intersectionClass);
	}

	private OWLClass getEquivalentClass (TriplesMap triplesMap, OWLClassExpression owlClassExpression) {
		Node<OWLClass> candidates = this.ontologyAndReasonerWrap.getEquivalentClasses(owlClassExpression);
		if (candidates.getSize() != 0)
			return candidates.getRepresentativeElement();
		return this.createEquivalentNewClass(triplesMap, owlClassExpression);
	}

	private OWLClass createEquivalentNewClass (TriplesMap triplesMap, OWLClassExpression owlClassExpression) {
		OWLClass resultingClassName = this.getFancyNewClass(triplesMap);
		OWLEquivalentClassesAxiom axiom = this.factory.getOWLEquivalentClassesAxiom(resultingClassName, owlClassExpression);
		this.ontologyAndReasonerWrap.addAxiom(axiom);
		return resultingClassName;
	}

	private void equivalentIntersection (OWLClassExpression classExpression, Set<? extends OWLClassExpression> intersection) {
		OWLEquivalentClassesAxiom axiom = this.factory.getOWLEquivalentClassesAxiom(classExpression, this.factory.getOWLObjectIntersectionOf(intersection));
		this.ontologyAndReasonerWrap.addAxiom(axiom);
	}

	private OWLClass getFancyNewClass (TriplesMap triplesMap) {
		String fancyNewIRI = Statics.sesameResourceToIRI(triplesMap).toString() + "_intersection_conjunction_" + this.classID++;
		return this.factory.getOWLClass(IRI.create(fancyNewIRI));
	}

	private OWLClassExpression getOWLClassExpression (OWLEntity owlProperty) {
		if (owlProperty instanceof OWLDataProperty)
			return this.factory.getOWLDataSomeValuesFrom((OWLDataProperty) owlProperty, this.factory.getTopDatatype());
		else if (owlProperty instanceof OWLDataPropertyExpression)
			return this.factory.getOWLDataSomeValuesFrom((OWLDataPropertyExpression) owlProperty, this.factory.getTopDatatype());
		else if (owlProperty instanceof OWLObjectProperty)
			return this.factory.getOWLObjectSomeValuesFrom((OWLObjectProperty) owlProperty, this.factory.getOWLThing());
		else if (owlProperty instanceof OWLObjectPropertyExpression)
			return this.factory.getOWLObjectSomeValuesFrom((OWLObjectPropertyExpression) owlProperty, this.factory.getOWLThing());
		this.log.warn("Checking disjunction with OWL Thing, unexpected class expression type: " + owlProperty.getClass().toString());
		return this.factory.getOWLThing();
	}

	private List<? extends OWLClassExpression> getInverseOWLClassExpressionMaybe (OWLEntity owlProperty) {
		if (owlProperty instanceof OWLDataProperty)
			return Arrays.asList();// this.factory.getOWLDataSomeValuesFrom((OWLDataProperty) owlProperty, this.factory.getTopDatatype()));
		else if (owlProperty instanceof OWLDataPropertyExpression)
			return Arrays.asList();// ;this.factory.getOWLDataSomeValuesFrom((OWLDataPropertyExpression) owlProperty, this.factory.getTopDatatype());
		else if (owlProperty instanceof OWLObjectProperty)
			return Arrays.asList(this.factory.getOWLObjectSomeValuesFrom(this.factory.getOWLObjectInverseOf((OWLObjectProperty) owlProperty), this.factory.getOWLThing()));
		else if (owlProperty instanceof OWLObjectPropertyExpression)
			return Arrays.asList(this.factory.getOWLObjectSomeValuesFrom(this.factory.getOWLObjectInverseOf((OWLObjectPropertyExpression) owlProperty), this.factory.getOWLThing()));
		this.log.warn("Checking disjunction with OWL Thing, unexpected class expression type: " + owlProperty.getClass().toString());
		return Arrays.asList();
	}

	public boolean subsumes (PredicateObjectMap pom1, PredicateObjectMap pom2, TriplesMap triplesMap1, TriplesMap triplesMap2) {
		OWLClass className = this.getEquivalentClass(triplesMap1, this.toIntersection(pom1, triplesMap1));
		return this.superclasses(this.toIntersection(pom2, triplesMap2)).containsEntity(className);
	}

	private NodeSet<OWLClass> superclasses (OWLClassExpression owlClassExpression) {
		return this.ontologyAndReasonerWrap.getSuperClasses(owlClassExpression);
	}

	private OWLClassExpression toIntersection (PredicateObjectMap pom, TriplesMap triplesMap) {
		return this.axiomsForJoin(this.scalaIsBetter(pom), triplesMap);
	}

	// def scalaIsBetter (predicateObjectMap: PredicateObjectMap) = predicateObjectMap.getPredicateMaps().map(TemplateAccessor).toSet
	private Set<TemplateAccessor> scalaIsBetter (PredicateObjectMap predicateObjectMap) {
		Set<TemplateAccessor> templateAccessors = new TreeSet<TemplateAccessor>();
		for (PredicateMap pm : predicateObjectMap.getPredicateMaps())
			templateAccessors.add(new TemplateAccessor(pm));
		return templateAccessors;
	}

}
