package eu.optique.api.component.omm.analysis.interfaces.optique;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.semanticweb.owlapi.model.OWLOntology;

import com.fluidops.iwb.datasource.metadata.Schema;

import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class AnalysisAPI {
	public static ArrayList<? extends HTMLSerializable> analyse (OWLOntology ontology, Collection<TriplesMap> mappings, List<Schema> schema) {
		Map<AnalysisType, Iterable<List<HTMLSerializable>>> results = BatchAnalyser.analyse(ontology, mappings, schema);
		ArrayList<HTMLSerializable> res = new ArrayList<HTMLSerializable>();
		for (Iterable<List<HTMLSerializable>> level : results.values())
			for (List<HTMLSerializable> list : level)
				res.addAll(list);
		return res;
	}

	public static ArrayList<? extends HTMLSerializable> analyseSyntactically (OWLOntology ontology, Collection<TriplesMap> mappings, List<Schema> schema) {
		ArrayList<TriplesMap> oderedMappings = new ArrayList<TriplesMap>(mappings);
		return SyntacticAnalyserImpl.analyse(oderedMappings, schema, ontology);
	}
}
