package eu.optique.api.component.omm.analysis.incremental;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import eu.optique.api.component.omm.analysis.MappingAnalysisOrderedResult;
import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedResultImpl;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class IncrementalHeadAnalyser {

	private ArrayList<IndividualHeadAnalyser> headAnalisers;
	private HeadConsistencyChecker headConsistencyChecker;
	private HeadSubsumptionChecker headSubsumptionChecker;

	public IncrementalHeadAnalyser(ArrayList<TriplesMap> triplesMaps, OWLOntology originalOntology) {
		this.init(originalOntology);
		for (TriplesMap triplesMap : triplesMaps)
			this.add(triplesMap);
	}

	public IncrementalHeadAnalyser(OWLOntology originalOntology) {
		this.init(originalOntology);
	}

	private OWLOntology init (OWLOntology originalOntology) {
		this.headAnalisers = new ArrayList<IndividualHeadAnalyser>();
		OWLOntology ontology = this.copyOntology(originalOntology);
		Disambiguator disambiguator = new Disambiguator(ontology);
		OntologyAndReasonerWrap ontologyAndReasonerWrap = new OntologyAndReasonerWrap(ontology);
		RoleIntersectionConjunctor iconjunctor = new RoleIntersectionConjunctor(ontologyAndReasonerWrap, disambiguator);
		this.headConsistencyChecker = new HermitRoleIntersectionConsistencyChecker(iconjunctor, ontologyAndReasonerWrap);
		this.headSubsumptionChecker = new HermitRoleIntersectionSubsumptionChecker(iconjunctor, ontologyAndReasonerWrap);
		return ontology;
	}

	private OWLOntology copyOntology (OWLOntology original) {
		try {
			return OWLManager.createOWLOntologyManager().createOntology(original.getAxioms());
		} catch (OWLOntologyCreationException e) {
			return original;
		}
	}

	public void add (TriplesMap triplesMap) {
		IndividualHeadAnalyser individualHeadAnalyser = new IndividualHeadAnalyser(triplesMap, this.headSubsumptionChecker, this.headConsistencyChecker);
		// TODO: maybe some other things should be done here for the individual head analyser, that depends on how it may use the other objects...
		this.headAnalisers.add(individualHeadAnalyser);
		individualHeadAnalyser.analyseWRT(this.headAnalisers, this.headAnalisers.size() - 1);
	}

	public Set<MappingAnalysisOrderedResult> getResults () {
		Set<MappingAnalysisOrderedResult> res = new HashSet<MappingAnalysisOrderedResult>();
		HashSet<Integer> subject;
		Set<Integer> subsumed;
		ArrayList<TriplesMap> ordering = this.getTriplesMaps(this.headAnalisers);
		for (IndividualHeadAnalyser ha : this.headAnalisers) {
			subject = new HashSet<Integer>();
			subject.add(ha.getId());
			if (!ha.isConsistent())
				res.add(new MappingAnalysisOrderedResultImpl(subject, MappingAnalysisRelation.HEAD_INCONSISTENT, new HashSet<Integer>(), ordering));
			subsumed = ha.getSubsumedIndexes();
			if (!subsumed.isEmpty())
				res.add(new MappingAnalysisOrderedResultImpl(subject, MappingAnalysisRelation.HEAD_SUBSUMES, subsumed, ordering));
		}
		return res;
	}

	private ArrayList<TriplesMap> getTriplesMaps (ArrayList<IndividualHeadAnalyser> headAnalysers) {
		ArrayList<TriplesMap> res = new ArrayList<TriplesMap>();
		for (IndividualHeadAnalyser individualHeadAnalyser : headAnalysers)
			res.add(individualHeadAnalyser.getTriplesMap());
		return res;
	}

}
