package eu.optique.api.component.omm.analysis;

import java.util.Collection;

import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 * @param <T>
 */
public interface MappingAnalysisResult<T> extends HTMLSerializable {

	public Collection<TriplesMap> getSubject ();

	public MappingAnalysisRelation getPredicate ();

	public Collection<T> getObject ();

}
