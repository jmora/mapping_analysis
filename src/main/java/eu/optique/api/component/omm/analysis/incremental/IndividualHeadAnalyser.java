package eu.optique.api.component.omm.analysis.incremental;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 */
public class IndividualHeadAnalyser {
	private boolean consistent;
	private ArrayList<IndividualHeadAnalyser> ordering = null;
	private int index;
	private Set<SubsumptionResults> subsumed = new HashSet<SubsumptionResults>();
	private TriplesMap containedTriplesMap;
	private HeadSubsumptionChecker subsumptionChecker;
	private Log log = LogFactory.getLog(IndividualHeadAnalyser.class);

	public IndividualHeadAnalyser(TriplesMap containedTriplesMap, HeadSubsumptionChecker subsumptionChecker, HeadConsistencyChecker consistencyChecker) {
		this.containedTriplesMap = containedTriplesMap;
		this.consistent = consistencyChecker.isHeadConsistent(containedTriplesMap);
		this.subsumptionChecker = subsumptionChecker;
	}

	private static class SubsumptionResults {
		private int index;

		private TriplesMap subsumedTriplesMap;
		private List<String> thisVariableOrder;
		private List<String> thatVariableOrder;

		public SubsumptionResults(int index, TriplesMap subsumedTriplesMap, List<String> thisVariableOrder, List<String> thatVariableOrder) {
			this.index = index;
			this.subsumedTriplesMap = subsumedTriplesMap;
			this.thisVariableOrder = thisVariableOrder;
			this.thatVariableOrder = thatVariableOrder;
		}

		public int getIndex () {
			return this.index;
		}

		public TriplesMap getSubsumedTriplesMap () {
			return this.subsumedTriplesMap;
		}

		public List<String> getThisVariableOrder () {
			return this.thisVariableOrder;
		}

		public List<String> getThatVariableOrder () {
			return this.thatVariableOrder;
		}
	}

	public int getId () {
		return this.index;
	}

	public boolean isConsistent () {
		return this.consistent;
	}

	public void analyseWRT (ArrayList<IndividualHeadAnalyser> otherContainers, int yourIndex) {
		if (this.ordering != null)
			return; // TODO raise exception
		if (!this.isConsistent())
			return;
		this.ordering = otherContainers;
		this.index = yourIndex;
		for (int i = this.ordering.size() - 1; i >= 0; i--)
			if (i != yourIndex) { // We compare both directions, (it's incremental! Thus, yourIndex should be the last one, but shit happens often times)
				IndividualHeadAnalyser that = this.ordering.get(i);
				if (!that.isConsistent())
					continue;
				this.actIfSubsumes(this, that);
				this.actIfSubsumes(that, this);
			}
	}

	public TriplesMap getTriplesMap () {
		return this.containedTriplesMap;
	}

	public Set<TriplesMap> getSubsumed () {
		Set<TriplesMap> res = new HashSet<TriplesMap>();
		for (SubsumptionResults s : this.subsumed)
			res.add(s.getSubsumedTriplesMap());
		return res;
	}

	public Set<Integer> getSubsumedIndexes () {
		Set<Integer> res = new HashSet<Integer>();
		for (SubsumptionResults s : this.subsumed)
			res.add(s.getIndex());
		return res;
	}

	private void actIfSubsumes (IndividualHeadAnalyser subsuming, IndividualHeadAnalyser subsumed) {
		List<List<String>> subsumptionResult = this.subsumptionChecker.headSubsumesAsLists(subsuming.getTriplesMap(), subsumed.getTriplesMap());
		if (subsumptionResult == null)
			return;
		if (this.ordering.size() > subsuming.ordering.size()) // TODO: we are assuming here that no mappings are deleted; that could cause much trouble.
			subsuming.ordering = this.ordering;
		else if (this.ordering.size() < subsuming.ordering.size())
			this.ordering = subsuming.ordering;
		// this.print(subsuming, subsumed, subsumptionResult);
		subsuming.subsumed.add(new SubsumptionResults(subsumed.index, subsumed.getTriplesMap(), subsumptionResult.get(0), subsumptionResult.get(1)));
	}

	// TODO: delete this method, please
	/*
	 * private void print (IndividualHeadAnalyser subsuming, IndividualHeadAnalyser subsumed2, List<List<String>> subsumptionResult) { System.out.println();
	 * System.out.println(Statics.sesameResourceToURI(subsuming.getTriplesMap()) + "\t->\t" + Statics.sesameResourceToURI(subsumed2.getTriplesMap())); for (int i = 0; i <
	 * subsumptionResult.get(0).size(); i++) System.out.println(subsumptionResult.get(0).get(i) + "\t->\t" + subsumptionResult.get(1).get(i)); }
	 */

}
