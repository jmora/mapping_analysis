package eu.optique.api.component.omm.analysis.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.semanticweb.owlapi.model.OWLOntology;

import eu.optique.api.component.omm.analysis.HeadAnalyser;
import eu.optique.api.component.omm.analysis.MappingAnalysisOrderedResult;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class HeadAnalyserImpl implements HeadAnalyser {
	private OWLOntology ontology;
	private ArrayList<TriplesMap> mappings;
	private Hierarchy<Integer> classification;
	private Collection<MappingAnalysisOrderedResult> orderedResults;

	public HeadAnalyserImpl(Collection<TriplesMap> mappings, OWLOntology ontology) {
		this.mappings = new ArrayList<TriplesMap>(mappings);
		this.ontology = ontology;
		this.classification = null;
		this.orderedResults = null;
	}

	@Override
	public Collection<MappingAnalysisOrderedResult> getOrderedResults () {
		if (this.orderedResults == null)
			this.calculateOrderedResults();
		return this.orderedResults;
	}

	// Version info: there are no more results than the classification in this prototype
	private void calculateOrderedResults () {
		this.orderedResults = new ArrayList<MappingAnalysisOrderedResult>();
	}

	@Override
	public Hierarchy<Integer> getClassification () {
		if (this.classification == null)
			this.calculateClassification();
		return this.classification;
	}

	private void calculateClassification () {
		this.classification = new HeadClassifier(this.ontology).getClassification(this.mappings);
	}
}
