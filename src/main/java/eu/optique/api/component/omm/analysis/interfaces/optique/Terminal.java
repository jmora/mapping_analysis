package eu.optique.api.component.omm.analysis.interfaces.optique;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.List;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.fluidops.iwb.datasource.metadata.Schema;
import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;

import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class Terminal {

	// Parameters: ontology, mappings file, schema file
	public static void main (String[] args) throws OWLOntologyCreationException, InvalidR2RMLMappingException, RDFParseException, UnsupportedRDFormatException, FileNotFoundException,
			MalformedURLException, IOException, InvalidSchemaSpecificationException {
		TerminalHelper helper = new TerminalHelper();
		OWLOntology ontology = helper.getOntology(args[0]);
		Collection<TriplesMap> mappings = helper.getMappings(args[1]);
		URI metaDataURI = new URIImpl("http://www.optique-project.eu/mapping-analysis/base/");
		if (args.length > 3)
			metaDataURI = new URIImpl(args[3]);
		List<Schema> schema = helper.getSchema(args[2], metaDataURI);
		for (HTMLSerializable result : AnalysisAPI.analyseSyntactically(ontology, mappings, schema))
			System.out.println(result.toNamedString());
		// Not yet... for (MappingAnalysisResult result : AnalysisAPI.analyse(ontology, mappings, schema)) System.out.println(result.toString());
	}

}
