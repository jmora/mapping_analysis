/**
 * 
 */
package eu.optique.api.component.omm.analysis.interfaces.optique;

/**
 * Enumerates the possible types of mapping analysis provided by the mapping analysis module
 * 
 * @author jmora
 * 
 */
public enum AnalysisType {

	/**
	 * Identifies the syntactic type of mapping analysis
	 */
	Syntactic("Syntactic"),

	/**
	 * Identifies the consistency type of mapping analysis: it is a semantic check which verifies whether each mapping assertion is consistent
	 */
	Consistency("Consistency"),

	/**
	 * Identifies the consistency type of mapping analysis: it is a semantic check which given every possible pair of mapping assertions m_1 and m_2 verifies whether m_1 subsumes m_2 or viceversa
	 */
	Subsumption("Subsumption");

	private final String name;

	public String message () {
		return this.name;
	}

	AnalysisType(String name) {
		this.name = name;
	}
}
