/**
 * 
 */
package eu.optique.api.component.omm.analysis.incremental;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.mapping.ObjectMap;
import eu.optique.api.mapping.PredicateObjectMap;
import eu.optique.api.mapping.TermMap;
import eu.optique.api.mapping.TermMap.TermMapType;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.TemplateAccessor;

/**
 * @author jmora
 * 
 */
public class Unifier {

	/*
	 * public static List<List<String>> getUnification (TriplesMap triplesMap1, TriplesMap triplesMap2, RoleIntersectionConjunctor conjunctor) { return new Unifier(triplesMap1, triplesMap2,
	 * conjunctor).getResult(); }
	 */

	public boolean sameArity () {
		return this.pendingIn1.size() == this.pendingIn2.size();
	}

	public List<List<String>> getResult () {
		this.calculateUnificationIfNeeded();
		if (this.order1.size() == this.order2.size() && this.sameArity() && this.pendingIn1.size() == this.order1.size())
			return Arrays.asList(this.order1, this.order2);
		return null;
	}

	private synchronized void calculateUnificationIfNeeded () {
		if (this.attempted)
			return;
		this.attempted = true;
		if (!this.sameArity())
			return;
		if (this.unifyIfPossible(ThingToUnify.createThing(this.triplesMap1.getSubjectMap()), ThingToUnify.createThing(this.triplesMap2.getSubjectMap())))
			this.unifyPOMaps();
	}

	private TriplesMap triplesMap1;
	private TriplesMap triplesMap2;
	private Set<String> pendingIn1;
	private Set<String> pendingIn2;
	private List<String> order1;
	private List<String> order2;
	private RoleIntersectionConjunctor conjunctor;
	private boolean attempted = false;

	public Unifier(TriplesMap triplesMap1, TriplesMap triplesMap2, RoleIntersectionConjunctor conjunctor) {
		this.triplesMap1 = triplesMap1;
		this.triplesMap2 = triplesMap2;
		this.conjunctor = conjunctor;
		this.order1 = new ArrayList<String>();
		this.order2 = new ArrayList<String>();
		this.pendingIn1 = this.extractVariables(this.extractElements(triplesMap1));
		this.pendingIn2 = this.extractVariables(this.extractElements(triplesMap2));
	}

	private Set<String> extractVariables (ArrayList<ThingToUnify> extractElements) {
		Set<String> res = new HashSet<String>();
		for (ThingToUnify thing : extractElements)
			if (thing instanceof TemplateToUnify)
				res.addAll(this.extractVariables((TemplateToUnify) thing));
			else if (thing instanceof ColumnToUnify)
				res.add(((ColumnToUnify) thing).value);
		return res;
	}

	private Collection<? extends String> extractVariables (TemplateToUnify thing) {
		return thing.value.getVariables();
	}

	private void unifyPOMaps () {
		for (PredicateObjectMap pom : this.triplesMap1.getPredicateObjectMaps())
			this.findSubsumedAndUnify(pom);
	}

	private void findSubsumedAndUnify (PredicateObjectMap pom1) {
		for (PredicateObjectMap pom2 : this.triplesMap2.getPredicateObjectMaps())
			if (this.conjunctor.subsumes(pom1, pom2, this.triplesMap1, this.triplesMap2) && this.unify(pom1, pom2))
				break;
	}

	private boolean unify (PredicateObjectMap pom1, PredicateObjectMap pom2) {
		for (ObjectMap om1 : pom1.getObjectMaps()) {
			if (!this.needsUnification(om1))
				continue;
			boolean found = false;
			for (ObjectMap om2 : pom2.getObjectMaps()) {
				if (!this.needsUnification(om2)) // filter
					continue;
				if (this.unifyIfPossible(ThingToUnify.createThing(om1), ThingToUnify.createThing(om2))) {
					found = true;
					break;
				}
			}
			if (!found)
				return false;
		}
		return true;
	}

	private boolean needsUnification (TermMap termMap) {
		return !termMap.getTermMapType().equals(TermMapType.CONSTANT_VALUED);
	}

	private boolean unifyIfPossible (ThingToUnify thing1, ThingToUnify thing2) {
		if (!thing1.getClass().equals(thing2.getClass()))
			return false;
		if (thing1 instanceof ColumnToUnify)
			return this.unifyThingIfPossible((ColumnToUnify) thing1, (ColumnToUnify) thing2);
		if (thing1 instanceof TemplateToUnify)
			return this.unifyThingIfPossible((TemplateToUnify) thing1, (TemplateToUnify) thing2);
		return false;
	}

	private boolean unifyThingIfPossible (TemplateToUnify template1, TemplateToUnify template2) {
		if (!template1.getFunction().equalsIgnoreCase(template2.getFunction()))
			return false;
		ArrayList<String> variables1 = template1.getValue().getVariables();
		ArrayList<String> variables2 = template2.getValue().getVariables();
		if (variables1.size() != variables2.size())
			return false;
		for (int i = 0; i < variables1.size(); i++)
			if (!this.unifyThingIfPossible(variables1.get(i), variables2.get(i)))
				return false;
		return true;
	}

	private boolean unifyThingIfPossible (ColumnToUnify column1, ColumnToUnify column2) {
		return this.unifyThingIfPossible(column1.getValue(), column2.getValue());
	}

	private boolean unifyThingIfPossible (String string1, String string2) {
		if (this.order1.contains(string1) != this.order2.contains(string2))
			return false;
		if (this.order1.contains(string1))
			return this.order2.get(this.order1.indexOf(string1)).equals(string2);
		this.order1.add(string1);
		this.order2.add(string2);
		return true;
	}

	private ArrayList<ThingToUnify> extractElements (TriplesMap triplesMap) {
		ArrayList<ThingToUnify> allEnds = new ArrayList<ThingToUnify>();
		for (PredicateObjectMap pom : triplesMap.getPredicateObjectMaps()) {
			ArrayList<ThingToUnify> paths = this.makeThingsToUnify(pom.getPredicateMaps());
			ArrayList<ThingToUnify> ends = this.makeThingsToUnify(pom.getObjectMaps());
			for (ThingToUnify p : paths)
				p.setDependentArtifacts(ends);
			for (ThingToUnify e : ends)
				e.setDependentArtifacts(paths);
			allEnds.addAll(ends);
		}
		allEnds.addAll(this.makeThingsToUnify(Arrays.asList(triplesMap.getSubjectMap())));
		return allEnds;
	}

	private ArrayList<ThingToUnify> makeThingsToUnify (List<? extends TermMap> termMaps) {
		ArrayList<ThingToUnify> res = new ArrayList<ThingToUnify>();
		for (TermMap termMap : termMaps)
			res.add(ThingToUnify.createThing(termMap));
		return res;
	}

	// these are awesome case classes
	private static class ThingToUnify {
		private ArrayList<ThingToUnify> dependentArtifacts;

		public static ThingToUnify createThing (TermMap termMap) {
			switch (termMap.getTermMapType()) {
			case COLUMN_VALUED:
				return new ColumnToUnify(termMap.getColumn());
			case CONSTANT_VALUED:
				return new ConstantToUnify(termMap.getConstant());
			case TEMPLATE_VALUED:
				return new TemplateToUnify(new TemplateAccessor(termMap));
			default:
				return null;
			}
		}

		public ArrayList<ThingToUnify> getDependentArtifacts () {
			return this.dependentArtifacts;
		}

		public void setDependentArtifacts (ArrayList<ThingToUnify> dependentArtifacts) {
			this.dependentArtifacts = dependentArtifacts;
		}

	}

	private static class TemplateToUnify extends ThingToUnify {

		private TemplateAccessor value;

		public TemplateToUnify(TemplateAccessor templateAccessor) {
			this.value = templateAccessor;
		}

		public String getFunction () {
			return this.value.getAsString();
		}

		public TemplateAccessor getValue () {
			return this.value;
		}
	}

	private static class ColumnToUnify extends ThingToUnify {

		private String value;

		public ColumnToUnify(String string) {
			this.value = string;
		}

		public String getValue () {
			return this.value;
		}

	}

	// well, this one is easy
	private static class ConstantToUnify extends ThingToUnify {

		private String value;

		public ConstantToUnify(String string) {
			this.value = Statics.correctCase(string);
		}

		public String getValue () {
			return this.value;
		}
	}

}
