package eu.optique.api.component.omm.analysis.syntax;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

import com.fluidops.iwb.datasource.metadata.Column;
import com.fluidops.iwb.datasource.metadata.Table;

/**
 * @author jmora
 * 
 */
public class TableGetter implements Callable<Result> {
	private Table table;

	public TableGetter(Table table) {
		this.table = table;
	}

	@Override
	public Result call () throws Exception {
		Set<String> columns = new HashSet<String>();
		for (Column column : this.table.getColumns())
			columns.add(column.getName().toLowerCase());
		return new Result(this.table.getName().toLowerCase(), columns);
	}

}
