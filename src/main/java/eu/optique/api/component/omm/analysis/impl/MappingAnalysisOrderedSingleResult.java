package eu.optique.api.component.omm.analysis.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.MappingAnalysisResult;
import eu.optique.api.component.omm.analysis.MappingAnalysisSingleOrderedResult;
import eu.optique.api.component.omm.analysis.convenience.Globals;
import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class MappingAnalysisOrderedSingleResult implements MappingAnalysisSingleOrderedResult {

	private Set<Integer> subject;
	private MappingAnalysisRelation predicate;
	private Set<String> object;
	private ArrayList<TriplesMap> ordering;

	public MappingAnalysisOrderedSingleResult(Set<Integer> subject, MappingAnalysisRelation predicate, Set<String> object, ArrayList<TriplesMap> ordering) {
		super();
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
		this.ordering = ordering;
	}

	@Override
	public Set<Integer> getSubject () {
		return this.subject;
	}

	@Override
	public MappingAnalysisRelation getPredicate () {
		return this.predicate;
	}

	@Override
	public Set<String> getObject () {
		return this.object;
	}

	@Override
	public MappingAnalysisResult<String> toResult () {
		return new MappingAnalysisResultImpl<String>(this.toValue(this.subject), this.predicate, this.object);
	}

	private Set<TriplesMap> toValue (Set<Integer> indexes) {
		HashSet<TriplesMap> result = new HashSet<TriplesMap>();
		for (Integer i : indexes)
			result.add(this.ordering.get(i));
		return result;
	}

	public Set<String> toNames (Set<Integer> indexes) {
		HashSet<String> result = new HashSet<String>();
		for (Integer i : indexes)
			result.add(this.ordering.get(i).getResource(Globals.sesameResource).getLocalName().toString());
		return result;
	}

	public Set<String> toLinks (Set<Integer> indexes) {
		HashSet<String> result = new HashSet<String>();
		for (Integer i : indexes)
			result.add(Statics.triplesMapToAhref(this.ordering.get(i)));
		return result;
	}

	@Override
	public String toHTML () {
		return Statics.mkstring(this.toLinks(this.subject), ", ", "{", "} ") + this.predicate.message() + Statics.mkstring(this.object, ", ", "{", "}");
	}

	@Override
	public String toNamedString () {
		return Statics.mkstring(this.toNames(this.subject), ", ", "{", "} ") + this.predicate.message() + Statics.mkstring(this.object, ", ", "{", "}");
	}

}
