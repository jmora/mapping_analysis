/**
 * 
 */
package eu.optique.api.component.omm.analysis.lazy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;
import eu.optique.api.component.omm.analysis.interfaces.optique.SingleAnalyser;

/**
 * @author jmora
 * 
 */
public class ConsistencyResultsIterable implements Iterable<List<HTMLSerializable>> {

	private ArrayList<SingleAnalyser> analysers;

	public ConsistencyResultsIterable(ArrayList<SingleAnalyser> analysers) {
		this.analysers = analysers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<List<HTMLSerializable>> iterator () {
		return new ConsistencyResultsIterator(this.analysers.iterator());
	}

}
