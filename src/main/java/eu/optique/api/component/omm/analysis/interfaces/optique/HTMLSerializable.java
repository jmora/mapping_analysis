package eu.optique.api.component.omm.analysis.interfaces.optique;

/**
 * @author jmora
 * 
 */
public interface HTMLSerializable {

	public String toHTML ();

	public String toNamedString ();

}
