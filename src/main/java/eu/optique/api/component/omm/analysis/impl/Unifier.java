package eu.optique.api.component.omm.analysis.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author jmora
 * 
 */
public class Unifier {

	private HashMap<String, String> unifications;

	public Unifier() {
		this.unifications = new HashMap<String, String>();
	}

	public Unifier(Map<String, String> unifications) {
		unifications = new HashMap<String, String>(unifications);
	}

	public Unifier(Unifier unifier) {
		this.unifications = new HashMap<String, String>(unifier.unifications);
	}

	public Map<String, String> getUnifications () {
		return this.unifications;
	}

	public void add (String from, String to) {
		this.unifications.put(from, to);
	}

	public boolean compatible (Unifier unifier) {
		for (Entry<String, String> e : unifier.unifications.entrySet())
			if (this.unifications.containsKey(e.getValue()) && this.unifications.get(e.getKey()) != e.getValue())
				return false;
		return true;
	}

	public Unifier merge (Unifier unifier) {
		HashMap<String, String> r = new HashMap<String, String>(this.unifications);
		r.putAll(unifier.unifications);
		return new Unifier(r);
	}

	public Unifier merge (HashMap<String, String> unifications) {
		HashMap<String, String> r = new HashMap<String, String>(this.unifications);
		r.putAll(unifications);
		return new Unifier(r);
	}

}
