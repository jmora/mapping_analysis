package eu.optique.api.component.omm.analysis;

import java.util.Set;

import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public interface MappingAnalysisOrderedResult {

	public Set<Integer> getSubject ();

	public MappingAnalysisRelation getPredicate ();

	public Set<Integer> getObject ();

	public MappingAnalysisResult<TriplesMap> toResult ();

}
