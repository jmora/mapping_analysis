/**
 * 
 */
package eu.optique.api.component.omm.analysis.convenience;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;

import com.fluidops.iwb.datasource.metadata.Schema;
import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;

import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper;
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl;

/**
 * @author jmora
 * 
 *         TODO: destroy this
 */
public class SchemaToFile {

	/**
	 * @param args
	 * @throws InvalidSchemaSpecificationException
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws FileNotFoundException
	 * @throws UnsupportedRDFormatException
	 * @throws RDFParseException
	 */
	public static void main (String[] args) throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException, InvalidSchemaSpecificationException {
		String inputPath = "resources/statoilSchema.rdf";
		String outputPath;
		if (args.length > 0)
			inputPath = args[0];
		if (args.length > 1)
			outputPath = args[1];
		else
			outputPath = inputPath.substring(0, inputPath.lastIndexOf('.')) + "-Simple.schema";
		TerminalHelper helper = new TerminalHelper();
		URI metaDataURI = new URIImpl("http://www.optique-project.eu/mapping-analysis/base/");
		List<Schema> schemata = helper.getSchema(inputPath, metaDataURI);
		try (PrintStream outfile = new PrintStream(outputPath)) {
			for (Schema schema : schemata)
				SchemaToFile.print(outfile, schema.getName(), SyntacticAnalyserImpl.getSignature(Arrays.asList(schema)));
		}
	}

	private static void print (PrintStream outfile, String schema, HashMap<String, Set<String>> tables) {
		for (Entry<String, Set<String>> table : tables.entrySet())
			outfile.println(schema + "." + table.getKey() + Statics.mkstring(table.getValue(), ",", "(", ")"));
	}

}
