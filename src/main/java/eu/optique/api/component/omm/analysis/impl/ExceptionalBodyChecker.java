/**
 * 
 */
package eu.optique.api.component.omm.analysis.impl;

import java.util.Map;

import eu.optique.api.component.omm.analysis.exceptions.BodyCheckerException;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class ExceptionalBodyChecker extends eu.optique.api.component.omm.analysis.impl.bodychecker.BodyChecker {

	private BodyCheckerException e;

	public ExceptionalBodyChecker(BodyCheckerException exception) {
		this.e = exception;
	}

	@Override
	public boolean checkBodySubsumption (TriplesMap body1, TriplesMap body2, Map<String, String> unification) throws BodyCheckerException {
		throw this.e;
	}

	@Override
	public boolean checkBodyConsistency (TriplesMap body) throws BodyCheckerException {
		throw this.e;
	}

}
