package eu.optique.api.component.omm.analysis.impl;

import java.util.ArrayList;
import java.util.List;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.RDFResource;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import eu.optique.api.component.omm.analysis.convenience.Globals;
import eu.optique.api.mapping.ObjectMap;
import eu.optique.api.mapping.PredicateMap;
import eu.optique.api.mapping.PredicateObjectMap;
import eu.optique.api.mapping.SubjectMap;
import eu.optique.api.mapping.TermMap;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.TemplateAccessor;

/**
 * @author jmora
 * 
 */
public class HeadClassifier {

	static public final int NORELATION = 0; // absorbing element, do not change
	static public final int EQUIVALENT = 1; // neutral element, do not change
	static public final int SUBSUMES = 2; // extend only with prime numbers
	static public final int SUBSUMEDBY = 3; // check checkSubsumption method
	private OWLOntology ontology;
	private OWLReasoner reasoner;
	private final OWLDataFactory dataFactory = OWLManager.createOWLOntologyManager().getOWLDataFactory();

	public HeadClassifier(OWLOntology ontology) {
		this.ontology = ontology;
		this.reasoner = new Reasoner.ReasonerFactory().createReasoner(ontology);
		this.reasoner.precomputeInferences();
	}

	Hierarchy<Integer> getClassification (ArrayList<TriplesMap> mappings) {
		Hierarchy<Integer> result = new Hierarchy<Integer>();
		for (int i = 0; i < mappings.size(); i++)
			for (int j = i + 1; j < mappings.size(); j++)
				switch (this.checkSubsumption(mappings.get(i), mappings.get(j))) {
				case SUBSUMES:
					result.addChild(i, j);
					break;
				case SUBSUMEDBY:
					result.addChild(j, i);
					break;
				case NORELATION:
					break;
				case EQUIVALENT:
					result.addChild(i, j);
					result.addChild(j, i);
					break;
				default:
					break;
				}
		return result;
	}

	private int checkSubsumption (TriplesMap triplesMap, TriplesMap triplesMap2) {
		int subjectsResult = this.checkSubjects(triplesMap.getSubjectMap(), triplesMap2.getSubjectMap());
		if (subjectsResult == HeadClassifier.NORELATION)
			return subjectsResult;
		int predicateObjectsResult = this.checkPredicateObjecMaps(triplesMap.getPredicateObjectMaps(), triplesMap2.getPredicateObjectMaps());
		switch (subjectsResult * predicateObjectsResult) {
		case 1:
			return HeadClassifier.EQUIVALENT;
		case 2:
			return HeadClassifier.SUBSUMES;
		case 3:
			return HeadClassifier.SUBSUMEDBY;
		case 4:
			return HeadClassifier.SUBSUMES;
		case 9:
			return HeadClassifier.SUBSUMEDBY;
		default:
			return HeadClassifier.NORELATION;
		}
	}

	// I'd have liked higher order for this
	private int checkPredicateObjecMaps (List<PredicateObjectMap> predicateObjectMaps, List<PredicateObjectMap> predicateObjectMaps2) {
		boolean subsumes = this.headSubsumesPredicates(predicateObjectMaps, predicateObjectMaps2);
		boolean subsumedBy = this.headSubsumesPredicates(predicateObjectMaps2, predicateObjectMaps);
		if (subsumes && subsumedBy)
			return HeadClassifier.EQUIVALENT;
		if (subsumes)
			return HeadClassifier.SUBSUMES;
		if (subsumedBy)
			return HeadClassifier.SUBSUMEDBY;
		return HeadClassifier.NORELATION;
	}

	private boolean headSubsumesPredicates (List<PredicateObjectMap> predicateObjectMaps, List<PredicateObjectMap> predicateObjectMaps2) {
		for (PredicateObjectMap predicateObjectMap : predicateObjectMaps)
			for (PredicateMap predicateMap : predicateObjectMap.getPredicateMaps())
				for (ObjectMap objectMap : predicateObjectMap.getObjectMaps())
					if (!this.generalizesAny(predicateMap, objectMap, predicateObjectMaps2))
						return false;
		return true;
	}

	private boolean generalizesAny (PredicateMap predicateMap, ObjectMap objectMap, List<PredicateObjectMap> predicateObjectMaps2) {
		for (PredicateObjectMap predicateObjectMap2 : predicateObjectMaps2)
			for (PredicateMap predicateMap2 : predicateObjectMap2.getPredicateMaps())
				for (ObjectMap objectMap2 : predicateObjectMap2.getObjectMaps())
					if (this.generalizesOne(predicateMap, objectMap, predicateMap2, objectMap2))
						return true;
		return false;
	}

	private boolean generalizesOne (PredicateMap predicateMap, ObjectMap objectMap, PredicateMap predicateMap2, ObjectMap objectMap2) {
		return false;
	}

	private int checkSubjects (SubjectMap subjectMap, SubjectMap subjectMap2) {
		subjectMap.getClasses(Globals.owlAPIResource);
		if (!this.equivalentTermMaps(subjectMap, subjectMap2))
			return HeadClassifier.NORELATION;
		return this.checkClassLists(this.getOWLClasses(subjectMap), this.getOWLClasses(subjectMap));
	}

	private int checkClassLists (ArrayList<OWLClass> owlClasses, ArrayList<OWLClass> owlClasses2) {
		boolean subsumes = this.headSubsumesClasses(owlClasses, owlClasses2);
		boolean subsumedBy = this.headSubsumesClasses(owlClasses2, owlClasses);
		if (subsumes && subsumedBy)
			return HeadClassifier.EQUIVALENT;
		if (subsumes)
			return HeadClassifier.SUBSUMES;
		if (subsumedBy)
			return HeadClassifier.SUBSUMEDBY;
		return HeadClassifier.NORELATION;
	}

	private boolean headSubsumesClasses (ArrayList<OWLClass> owlClasses, ArrayList<OWLClass> owlClasses2) {
		boolean foundImplication = false;
		for (OWLClass c1 : owlClasses) {
			if (owlClasses2.contains(c1)) // all are implied if...
				continue;
			foundImplication = false;
			for (OWLClass sc : this.reasoner.getSubClasses(c1, false).getFlattened())
				if (owlClasses2.contains(sc)) { // ...for each one, any implies it.
					foundImplication = true;
					break;
				}
			if (!foundImplication)
				return false;
		}
		return true;
	}

	private ArrayList<OWLClass> getOWLClasses (SubjectMap subjectMap) {
		ArrayList<OWLClass> result = new ArrayList<OWLClass>();
		// It would be really beautiful it we could do this, but the subjectMap is done according to Sesame and this will fail:
		for (RDFResource c : subjectMap.getClasses(Globals.owlAPIResource))
			result.add(this.dataFactory.getOWLClass(c.getResource()));
		// TODO: this would be a perfect moment to check whether those classes are defined in the ontology
		return result;
	}

	private boolean equivalentTermMaps (TermMap termMap, TermMap termMap2) {
		if (termMap.getTermMapType() != termMap2.getTermMapType())
			return false;
		switch (termMap.getTermMapType()) {
		case COLUMN_VALUED:
			return true;
		case CONSTANT_VALUED:
			return termMap.getConstant().equals(termMap2.getConstant());
		case TEMPLATE_VALUED:
			return this.equivalentTemplates(termMap, termMap2);
		default:
			return false;
		}

	}

	private boolean equivalentTemplates (TermMap termMap, TermMap termMap2) {
		return new TemplateAccessor(termMap).equivalent(termMap2);
	}

}
