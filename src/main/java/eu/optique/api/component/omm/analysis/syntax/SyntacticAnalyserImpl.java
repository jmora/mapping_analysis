package eu.optique.api.component.omm.analysis.syntax;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import net.sf.jsqlparser.JSQLParserException;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;

import com.fluidops.iwb.datasource.metadata.Schema;
import com.fluidops.iwb.datasource.metadata.Table;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.convenience.FileLineIterator;
import eu.optique.api.component.omm.analysis.convenience.PoolManager;
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedSingleResult;
import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;
import eu.optique.api.component.omm.analysis.sqlapi.SQLPartialAPI;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class SyntacticAnalyserImpl {
	static final public String CLASS = "class";
	static final public String ROLE = "role";
	static final public String ATTRIBUTE = "attribute";

	// This is not OOP...
	public static ArrayList<HTMLSerializable> analyse (ArrayList<TriplesMap> orderedMappings, List<Schema> schema, OWLOntology ontology) {
		HashMap<String, Set<String>> signature = SyntacticAnalyserImpl.getSignature(schema);
		HashMap<String, Set<String>> ontologySignature = SyntacticAnalyserImpl.getSignature(ontology);
		ArrayList<MappingAnalysisOrderedSingleResult> result = new ArrayList<MappingAnalysisOrderedSingleResult>();
		TriplesMap mapping;
		SQLPartialAPI query;
		HashSet<Integer> nonParseable = new HashSet<Integer>();
		for (int i = 0; i < orderedMappings.size(); i++) {
			mapping = orderedMappings.get(i);
			try {
				query = new SQLPartialAPI(mapping);
				BodyChecker bc = BodyChecker.makeBodyChecker(orderedMappings, i, signature, query);
				result.addAll(bc.check());
				result.addAll(HeadChecker.check(orderedMappings, i, ontologySignature));
				// if (result.isEmpty())
				result.addAll(FrontierChecker.check(orderedMappings, i, signature, bc));
			} catch (JSQLParserException e) {
				nonParseable.add(i);
			}
		}
		if (!nonParseable.isEmpty())
			result.add(new MappingAnalysisOrderedSingleResult(nonParseable, MappingAnalysisRelation.PARSER_ERROR, new HashSet<String>(Arrays.asList("Exception trying to parse the SQL query.")),
					orderedMappings));
		return SyntacticAnalyserImpl.toResult(result);
	}

	private HashMap<String, Set<String>> databaseSignature;
	private HashMap<String, Set<String>> ontologySignature;

	public SyntacticAnalyserImpl(OWLOntology ontology, List<Schema> schemata) {
		this.databaseSignature = SyntacticAnalyserImpl.getSignature(schemata);
		this.ontologySignature = SyntacticAnalyserImpl.getSignature(ontology);
	}

	public SyntacticAnalyserImpl(OWLOntology ontology, File schemata) {
		this.databaseSignature = SyntacticAnalyserImpl.getSignature(schemata);
		this.ontologySignature = SyntacticAnalyserImpl.getSignature(ontology);
	}

	// TODO: delete this method and all methods depending on it
	private static HashMap<String, Set<String>> getSignature (File schemata) {
		HashMap<String, Set<String>> res = new HashMap<String, Set<String>>();
		try (FileLineIterator iterator = new FileLineIterator(schemata)) {
			for (String line : iterator) {
				String[] parts = line.split("\\(");
				String table = parts[0].substring(parts[0].lastIndexOf('.') + 1, parts[0].length());
				HashSet<String> columns = new HashSet<String>(Arrays.asList(parts[1].split("\\)")[0].split(",")));
				res.put(table, columns);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	public ArrayList<? extends HTMLSerializable> analyse (TriplesMap triplesMap) {
		ArrayList<MappingAnalysisOrderedSingleResult> result = new ArrayList<MappingAnalysisOrderedSingleResult>();
		ArrayList<TriplesMap> orderedMappings = new ArrayList<TriplesMap>(Arrays.asList(triplesMap));
		HashSet<Integer> nonParseable = new HashSet<Integer>();
		for (int i = 0; i < orderedMappings.size(); i++) {
			TriplesMap mapping = orderedMappings.get(i);
			try {
				SQLPartialAPI query = new SQLPartialAPI(mapping);
				BodyChecker bc = BodyChecker.makeBodyChecker(orderedMappings, i, this.databaseSignature, query);
				result.addAll(bc.check());
				result.addAll(HeadChecker.check(orderedMappings, i, this.ontologySignature));
				result.addAll(FrontierChecker.check(orderedMappings, i, this.databaseSignature, bc));
			} catch (JSQLParserException e) {
				nonParseable.add(i);
			}
		}
		if (!nonParseable.isEmpty())
			result.add(new MappingAnalysisOrderedSingleResult(nonParseable, MappingAnalysisRelation.PARSER_ERROR, new HashSet<String>(Arrays.asList("Exception trying to parse the SQL query.")),
					orderedMappings));
		return SyntacticAnalyserImpl.toResult(result);
	}

	static private ArrayList<HTMLSerializable> toResult (ArrayList<MappingAnalysisOrderedSingleResult> result) {
		ArrayList<HTMLSerializable> r = new ArrayList<HTMLSerializable>();
		for (MappingAnalysisOrderedSingleResult pr : result)
			r.add(pr.toResult());
		return r;
	}

	static public HashMap<String, Set<String>> getSignature (List<Schema> schemas) {
		HashMap<String, Set<String>> result = new HashMap<String, Set<String>>();
		ArrayList<Future<Result>> futureResult = new ArrayList<Future<Result>>();
		try (PoolManager poolmanager = new PoolManager()) {
			ExecutorService pool = poolmanager.getPool();
			for (Schema schema : schemas)
				for (Table table : schema.getTables())
					futureResult.add(pool.submit(new TableGetter(table)));
			Result tresult;
			for (Future<Result> entry : futureResult)
				try {
					tresult = entry.get();
					result.put(tresult.getKey(), tresult.getValue());
					entry.cancel(true);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return result;
	}

	static public HashMap<String, Set<String>> getSignature (OWLOntology ontology) {
		HashMap<String, Set<String>> result = new HashMap<String, Set<String>>();
		HashSet<String> names = new HashSet<String>();
		for (OWLClass owlClass : ontology.getClassesInSignature())
			names.add(owlClass.getIRI().toString());
		result.put(SyntacticAnalyserImpl.CLASS, names);
		names = new HashSet<String>();
		for (OWLObjectProperty owlRole : ontology.getObjectPropertiesInSignature())
			names.add(owlRole.getIRI().toString());
		result.put(SyntacticAnalyserImpl.ROLE, names);
		names = new HashSet<String>();
		for (OWLDataProperty owlAttribute : ontology.getDataPropertiesInSignature())
			names.add(owlAttribute.getIRI().toString());
		result.put(SyntacticAnalyserImpl.ATTRIBUTE, names);
		return result;
	}
}
