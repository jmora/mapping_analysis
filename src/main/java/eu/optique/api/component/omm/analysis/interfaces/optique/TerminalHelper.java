package eu.optique.api.component.omm.analysis.interfaces.optique;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.List;

import org.openrdf.model.Graph;
import org.openrdf.model.Model;
import org.openrdf.model.URI;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import com.fluidops.iwb.datasource.metadata.Schema;
import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;

import eu.optique.api.mapping.R2RMLMappingManagerFactory;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class TerminalHelper {

	private OWLOntologyManager owlOntologyManager;

	public TerminalHelper() {
		this.owlOntologyManager = OWLManager.createOWLOntologyManager();
	}

	public OWLOntology getOntology (String ontologyPath) throws OWLOntologyCreationException {
		return this.owlOntologyManager.loadOntologyFromOntologyDocument(new File(ontologyPath));
	}

	public Collection<TriplesMap> getMappings (String mappingsPath) throws OWLOntologyCreationException, InvalidR2RMLMappingException, RDFParseException, UnsupportedRDFormatException,
			FileNotFoundException, MalformedURLException, IOException {
		// RDFGraph graph = new RDFTranslator(this.owlOntologyManager, this.getOntology(mappingsPath), true).getGraph();
		// return R2RMLMappingManagerFactory.getOWLAPIMappingManager().importMappings(graph);
		Graph graph = this.getSesameModel(mappingsPath);
		return R2RMLMappingManagerFactory.getSesameMappingManager().importMappings(graph);
	}

	private Model getSesameModel (String filePath) throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException {
		File rdfFile = new File(filePath);
		RDFFormat format = Rio.getParserFormatForFileName(filePath);
		return Rio.parse(new FileInputStream(rdfFile), rdfFile.toURI().toURL().toString(), format);
	}

	public List<Schema> getSchema (String filePath, URI metaDataURI) throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException,
			InvalidSchemaSpecificationException {
		MetaDataObject metaData = new MetaDataObject(metaDataURI);
		File metadataRDFFile = new File(filePath);
		metaData.createFromRDFFile(metadataRDFFile);
		List<Schema> res = metaData.getSchema();
		return res;
	}

	protected static class MetaDataObject extends com.fluidops.iwb.datasource.metadata.impl.RelationalMetaDataObjectImpl {

		public MetaDataObject(URI metaDataID) throws InvalidSchemaSpecificationException {
			super(metaDataID);
		}

		public void createFromRDFFile (File file) throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, InvalidSchemaSpecificationException,
				IOException {
			if (this.tableList == null)
				this.initializeFromGraph(this.getSesameModel(file), this.metaDataID);
		}

		@Override
		protected void initializeFromRdf (URI metaDataID) throws IllegalArgumentException, InvalidSchemaSpecificationException {
			// do nothing here
		};

		private Model getSesameModel (File rdfFile) throws RDFParseException, UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, IOException {
			// try to guess rdf format, use turtle as fallback
			RDFFormat format = Rio.getParserFormatForFileName(rdfFile.getAbsolutePath(), RDFFormat.TURTLE);
			return Rio.parse(new FileInputStream(rdfFile), rdfFile.toURI().toURL().toString(), format);
		}

	}
}
