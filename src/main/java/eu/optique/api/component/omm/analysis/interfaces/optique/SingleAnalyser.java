/**
 * 
 */
package eu.optique.api.component.omm.analysis.interfaces.optique;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.convenience.Globals;
import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.exceptions.BodyCheckerException;
import eu.optique.api.component.omm.analysis.impl.BodyChecker;
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisResultImpl;
import eu.optique.api.component.omm.analysis.incremental.HermitRoleIntersectionConsistencyChecker;
import eu.optique.api.component.omm.analysis.incremental.HermitRoleIntersectionSubsumptionChecker;
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class SingleAnalyser {

	private SyntacticAnalyserImpl syntacticAnalyser;
	private HermitRoleIntersectionConsistencyChecker headConsistencyChecker;
	private HermitRoleIntersectionSubsumptionChecker headSubsumptionChecker;
	private BodyChecker bodyChecker;
	private TriplesMap triplesMap;
	private List<HTMLSerializable> consistencyResults;
	private List<HTMLSerializable> syntacticErrors;
	private boolean isInconsistent;

	public SingleAnalyser(SyntacticAnalyserImpl syntacticAnalyser, HermitRoleIntersectionConsistencyChecker headConsistencyChecker, HermitRoleIntersectionSubsumptionChecker headSubsumptionChecker,
			BodyChecker bodyChecker, TriplesMap triplesMap) {
		this.syntacticAnalyser = syntacticAnalyser;
		this.headConsistencyChecker = headConsistencyChecker;
		this.headSubsumptionChecker = headSubsumptionChecker;
		this.bodyChecker = bodyChecker;
		this.triplesMap = triplesMap;
		this.consistencyResults = null;
		this.syntacticErrors = null;
	}

	public boolean isSyntacticallyCorrect () {
		this.calculateSyntacticErrorsIfNeeded();
		return this.syntacticErrors.isEmpty();
	}

	public List<HTMLSerializable> getSyntacticErrors () {
		this.calculateSyntacticErrorsIfNeeded();
		return this.syntacticErrors;
	}

	private synchronized void calculateSyntacticErrorsIfNeeded () {
		if (this.syntacticErrors != null)
			return;
		this.syntacticErrors = new ArrayList<HTMLSerializable>();
		this.syntacticErrors.addAll(this.syntacticAnalyser.analyse(this.triplesMap));
	}

	public boolean isConsistent () {
		this.calculateConsistencyErrorsIfNeeded();
		// return this.isSyntacticallyCorrect() && this.consistencyResults.isEmpty(); // Not this anymore, due to exceptions
		return !this.isInconsistent;
	}

	public List<HTMLSerializable> getConsistencyErrors () {
		this.calculateConsistencyErrorsIfNeeded();
		return this.consistencyResults;
	}

	private synchronized void calculateConsistencyErrorsIfNeeded () {
		if (this.consistencyResults != null)
			return;
		this.isInconsistent = false;
		this.consistencyResults = new ArrayList<HTMLSerializable>();
		if (!this.isSyntacticallyCorrect())
			return;
		if (!this.headConsistencyChecker.isHeadConsistent(this.triplesMap)) {
			this.consistencyResults.add(this.headInconsistentResult());
			this.isInconsistent = true;
		}
		try {
			if (!this.bodyChecker.isBodyConsistent(this.triplesMap)) {
				this.consistencyResults.add(this.bodyInconsistentResult());
				this.isInconsistent = true;
			}
		} catch (BodyCheckerException e) {
			this.consistencyResults.add(this.bodyExceptionalConsistency(e));
		}
		if (this.isInconsistent)
			this.consistencyResults.add(this.mappingInconsistentResult());
	}

	private HTMLSerializable bodyExceptionalConsistency (BodyCheckerException e) {
		return new MappingAnalysisResultImpl<String>(Arrays.asList(this.triplesMap), MappingAnalysisRelation.EXCEPTION_BODY_CONSISTENCY, SingleAnalyser.asList(e));
	}

	private HTMLSerializable mappingInconsistentResult () {
		return new MappingAnalysisResultImpl<Object>(Arrays.asList(this.triplesMap), MappingAnalysisRelation.MAPPING_INCONSISTENT, Arrays.asList());
	}

	private HTMLSerializable headInconsistentResult () {
		return new MappingAnalysisResultImpl<Object>(Arrays.asList(this.triplesMap), MappingAnalysisRelation.HEAD_INCONSISTENT, Arrays.asList());
	}

	private HTMLSerializable bodyInconsistentResult () {
		return new MappingAnalysisResultImpl<Object>(Arrays.asList(this.triplesMap), MappingAnalysisRelation.BODY_INCONSISTENT, Arrays.asList());
	}

	private boolean subsumes (SingleAnalyser that) throws BodyCheckerException {
		Map<String, String> renaming = that.headSubsumes(this);
		if (renaming == null)
			return false;
		return this.bodySubsumes(that, renaming);
	}

	private boolean bodySubsumes (SingleAnalyser that, Map<String, String> renaming) throws BodyCheckerException {
		return this.bodyChecker.subsumes(this.triplesMap, that.triplesMap, renaming);
	}

	private Map<String, String> headSubsumes (SingleAnalyser that) {
		return this.headSubsumptionChecker.headSubsumes(this.triplesMap, that.triplesMap);
	}

	public boolean isSubsumed (SingleAnalyser that) {
		try {
			return that.subsumes(this);
		} catch (BodyCheckerException e) {
			return false; // CWA
		}
	}

	public List<HTMLSerializable> bothDirectionsSubsumption (SingleAnalyser that) {
		ArrayList<HTMLSerializable> res = new ArrayList<HTMLSerializable>();
		if (!this.isConsistent() || !that.isConsistent() || !this.isSyntacticallyCorrect() || !that.isSyntacticallyCorrect())
			return res;
		Map<String, String> thisToThat = this.headSubsumes(that);
		Map<String, String> thatToThis = that.headSubsumes(this);
		if (thisToThat != null) {
			res.add(SingleAnalyser.headSubsumesResult(this, that));
			try {
				if (this.bodySubsumes(that, thisToThat))
					res.add(SingleAnalyser.bodySubsumesResult(this, that));
			} catch (BodyCheckerException e) {
				res.add(SingleAnalyser.bodyExceptionalSubsumption(this, that, e));
			}
			try {
				if (that.bodySubsumes(this, thisToThat)) {
					res.add(SingleAnalyser.bodySubsumesResult(that, this));
					res.add(SingleAnalyser.mappingSubsumesResult(that, this));
				}
			} catch (BodyCheckerException e) {
				res.add(SingleAnalyser.bodyExceptionalSubsumption(that, this, e));
			}
		}
		if (thatToThis != null) {
			res.add(SingleAnalyser.headSubsumesResult(that, this));
			try {
				if (that.bodySubsumes(this, thatToThis))
					res.add(SingleAnalyser.bodySubsumesResult(that, this));
			} catch (BodyCheckerException e) {
				res.add(SingleAnalyser.bodyExceptionalSubsumption(this, that, e));
			}
			try {
				if (this.bodySubsumes(that, thatToThis)) {
					res.add(SingleAnalyser.bodySubsumesResult(this, that));
					res.add(SingleAnalyser.mappingSubsumesResult(this, that));
				}
			} catch (BodyCheckerException e) {
				res.add(SingleAnalyser.bodyExceptionalSubsumption(that, this, e));
			}
		}
		return res;
	}

	private static HTMLSerializable bodyExceptionalSubsumption (SingleAnalyser singleAnalyser, SingleAnalyser that, BodyCheckerException e) {
		return new MappingAnalysisResultImpl<String>(Arrays.asList(singleAnalyser.triplesMap, that.triplesMap), MappingAnalysisRelation.EXCEPTION_BODY_SUBSUMPTION, SingleAnalyser.asList(e));
	}

	private static HTMLSerializable mappingSubsumesResult (SingleAnalyser that, SingleAnalyser singleAnalyser) {
		return new MappingAnalysisResultImpl<TriplesMap>(Arrays.asList(singleAnalyser.triplesMap), MappingAnalysisRelation.MAPPING_SUBSUMES, Arrays.asList(that.triplesMap));
	}

	private static HTMLSerializable bodySubsumesResult (SingleAnalyser singleAnalyser, SingleAnalyser that) {
		return new MappingAnalysisResultImpl<TriplesMap>(Arrays.asList(singleAnalyser.triplesMap), MappingAnalysisRelation.BODY_SUBSUMES, Arrays.asList(that.triplesMap));
	}

	private static HTMLSerializable headSubsumesResult (SingleAnalyser singleAnalyser, SingleAnalyser that) {
		return new MappingAnalysisResultImpl<TriplesMap>(Arrays.asList(singleAnalyser.triplesMap), MappingAnalysisRelation.HEAD_SUBSUMES, Arrays.asList(that.triplesMap));
	}

	private static List<String> asList (Throwable e) {
		ArrayList<String> res = new ArrayList<String>();
		if (e == null)
			return res;
		if (e.getMessage() != null)
			res.add(e.getMessage());
		if (Globals.demo)
			return res;
		if (e.getCause() != null)
			res.addAll(SingleAnalyser.asList(e.getCause()));
		if (e.getStackTrace() != null)
			res.add(Statics.mkstring(e.getStackTrace(), "\n", "", ""));
		return res;
	}

}
