/**
 * 
 */
package eu.optique.api.component.omm.analysis.incremental;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.optique.api.component.omm.analysis.convenience.MapConverter;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class HermitRoleIntersectionSubsumptionChecker implements HeadSubsumptionChecker {

	private RoleIntersectionConjunctor conjunctor;
	private OntologyAndReasonerWrap ontologyAndReasonerWrap;

	public HermitRoleIntersectionSubsumptionChecker(RoleIntersectionConjunctor conjunctor, OntologyAndReasonerWrap ontologyAndReasonerWrap) {
		this.conjunctor = conjunctor;
		this.ontologyAndReasonerWrap = ontologyAndReasonerWrap;
	}

	@Override
	public List<List<String>> headSubsumesAsLists (TriplesMap a, TriplesMap b) {
		Unifier unifier = new Unifier(a, b, this.conjunctor);
		if (unifier.sameArity() && this.ontologyAndReasonerWrap.isSubclassOf(this.conjunctor.getConjunction(b), this.conjunctor.getConjunction(a)))
			return unifier.getResult();
		return null;
	}

	@Override
	public Map<String, String> headSubsumes (TriplesMap a, TriplesMap b) {
		List<List<String>> r = this.headSubsumesAsLists(a, b);
		if (r == null)
			return null;
		if (r.isEmpty() || r.get(0).isEmpty())
			return new HashMap<String, String>();
		return new MapConverter<String, String>().getMap(r.get(0), r.get(1));
	}
}
