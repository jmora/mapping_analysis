/**
 * 
 */
package eu.optique.api.component.omm.analysis.convenience;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author jmora
 * 
 */
public class MapConverter<K, V> {

	public Map<K, V> getMap (Iterable<K> keys, Iterable<V> values) {
		HashMap<K, V> res = new HashMap<K, V>();
		Iterator<K> keysi = keys.iterator();
		Iterator<V> valuesi = values.iterator();
		while (keysi.hasNext() && valuesi.hasNext())
			res.put(keysi.next(), valuesi.next());
		return res;
	}

}
