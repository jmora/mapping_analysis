package eu.optique.api.component.omm.analysis;

import java.util.Collection;

import eu.optique.api.component.omm.analysis.impl.Hierarchy;

/**
 * @author jmora
 * 
 */
public interface HeadAnalyser {

	Hierarchy<Integer> getClassification ();

	Collection<MappingAnalysisOrderedResult> getOrderedResults ();

}
