package eu.optique.api.component.omm.analysis.sqlapi;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.SelectBody;
import eu.optique.api.component.omm.analysis.sqlapi.FromTableCaseClasses.Source;
import eu.optique.api.component.omm.analysis.sqlapi.SelectVariableCaseClasses.Variable;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class SQLPartialAPI {

	private Statement query;
	private List<String> internalVariables;
	private List<String> frontierVariables;
	private List<Variable> variables;
	private List<Source> bodyTables;
	private SelectBody subquery;

	public SQLPartialAPI(TriplesMap mapping) throws JSQLParserException {
		this.query = new CCJSqlParserManager().parse(new StringReader(mapping.getLogicalTable().getSQLQuery()));
	}

	public SQLPartialAPI(String sqlQuery) throws JSQLParserException {
		this.query = new CCJSqlParserManager().parse(new StringReader(sqlQuery));
	}

	public SQLPartialAPI(SelectBody sqlQuery) {
		this.subquery = sqlQuery;
	}

	private void calculateVariables () {
		if (this.query != null)
			this.variables = HeadVisitor.getVariables(this.query);
		if (this.subquery != null)
			this.variables = HeadVisitor.getVariables(this.subquery);
	}

	public List<Variable> getVariables () {
		if (this.variables == null)
			this.calculateVariables();
		return this.variables;
	}

	private List<String> calculateInternalVariables () {
		if (this.variables == null)
			this.calculateVariables();
		this.internalVariables = new ArrayList<String>();
		for (Variable v : this.getVariables())
			this.internalVariables.addAll(v.getVariables());
		return this.internalVariables;
	}

	private List<String> calculateFrontierVariables () {
		if (this.variables == null)
			this.calculateVariables();
		this.frontierVariables = new ArrayList<String>();
		for (Variable v : this.getVariables())
			this.frontierVariables.add(v.getAlias());
		return this.frontierVariables;
	}

	public List<String> getInternalVariables () {
		if (this.internalVariables == null)
			this.calculateInternalVariables();
		return this.internalVariables;
	}

	public List<String> getFrontierVariables () {
		if (this.frontierVariables == null)
			this.calculateFrontierVariables();
		return this.frontierVariables;
	}

	private void calculateBodyTables () {
		if (this.query != null)
			this.bodyTables = BodyVisitor.getBodyTables(this.query);
		if (this.subquery != null)
			this.bodyTables = BodyVisitor.getBodyTables(this.subquery);
	}

	public List<Source> getBodyTables () {
		if (this.bodyTables == null)
			this.calculateBodyTables();
		return this.bodyTables;
	}

	@Override
	public String toString () {
		if (this.query != null)
			return this.query.toString();
		if (this.subquery != null)
			return this.subquery.toString();
		return "<SQL Query>";
	}

}
