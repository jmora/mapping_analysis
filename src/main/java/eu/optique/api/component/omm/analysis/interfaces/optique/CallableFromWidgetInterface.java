package eu.optique.api.component.omm.analysis.interfaces.optique;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;

import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.fluidops.iwb.annotation.CallableFromWidget;
import com.fluidops.iwb.datasource.metadata.impl.InvalidSchemaSpecificationException;
import com.fluidops.iwb.datasource.metadata.impl.RelationalMetaDataObjectImpl;
import com.fluidops.iwb.util.RDFUtil;

import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.InvalidR2RMLMappingException;

/**
 * @author jmora
 * 
 */
public class CallableFromWidgetInterface {

	@CallableFromWidget
	public static String test (String mappingsURI, String schemaURI, String ontologyURI) throws InvalidSchemaSpecificationException, OWLOntologyCreationException, RDFParseException,
			UnsupportedRDFormatException, FileNotFoundException, MalformedURLException, InvalidR2RMLMappingException, IOException {
		TerminalHelper helper = new TerminalHelper();
		Collection<TriplesMap> mappings = helper.getMappings(mappingsURI);
		OWLOntology ontologyowl = helper.getOntology(ontologyURI);
		return CallableFromWidgetInterface.toHTML(AnalysisAPI.analyse(ontologyowl, mappings, new RelationalMetaDataObjectImpl(RDFUtil.fullUri(schemaURI)).getSchema()));
	}

	private static String toHTML (Collection<? extends HTMLSerializable> results) {
		StringBuffer res = new StringBuffer("<ul>");
		for (HTMLSerializable li : results)
			res.append("<li>" + li.toHTML() + "</li>");
		return res + "</ul>";
	}
}
