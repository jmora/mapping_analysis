package eu.optique.api.component.omm.analysis.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * @author jmora
 * 
 * @param <T>
 */
public class Hierarchy<T> {

	private HashMap<T, Collection<T>> hierarchy = new HashMap<T, Collection<T>>();

	public Collection<T> put (T key, Collection<T> value) {
		return this.hierarchy.put(key, value);
	}

	public void addChild (T key, T child) {
		Collection<T> current;
		if (!this.hierarchy.containsKey(key)) {
			current = new HashSet<T>();
			this.hierarchy.put(key, current);
		} else {
			current = this.hierarchy.get(key);
			if (current.contains(child))
				return;
		}
		current.add(child);
	}

	public Collection<T> remove (T key) {
		return this.hierarchy.remove(key);
	}

	public Collection<T> get (T key) {
		return this.hierarchy.get(key);
	}

	public Map<T, Collection<T>> asMap () {
		return this.hierarchy;
	}

	public boolean containsKey (T key) {
		return this.hierarchy.containsKey(key);
	}

	public Collection<T> get (T key, boolean direct) {
		if (direct)
			return this.hierarchy.get(key);
		return this.getRecursiveChildren(key, new HashSet<T>());
	}

	private Collection<T> getRecursiveChildren (T item, HashSet<T> explored) {
		HashSet<T> res = new HashSet<T>();
		if (explored.contains(item))
			return res;
		explored.add(item);
		Collection<T> children = this.hierarchy.get(item);
		res.addAll(children);
		for (T child : children)
			res.addAll(this.getRecursiveChildren(child, explored));
		return res;
	}

}
