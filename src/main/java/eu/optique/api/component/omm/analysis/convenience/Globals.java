package eu.optique.api.component.omm.analysis.convenience;

import org.openrdf.model.impl.BNodeImpl;
import org.openrdf.model.impl.URIImpl;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owlapi.io.RDFResource;

/**
 * @author jmora
 * 
 */
public class Globals {
	// Yes, I'm very sorry that I have globals

	static public final Class<RDFResource> owlAPIResource = RDFResource.class;
	static public final Class<URIImpl> sesameResource = URIImpl.class;
	static public final Class<BNodeImpl> sesameAnonymousResource = BNodeImpl.class;
	static public final boolean caseinsensitiveness = true; // this is the best we can have until R2RMLAPI informs about what is a regular or a delimited identifier
	static public final PoolManager poolmanager = new PoolManager();
	static public final ReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
	public static final int conjunctionType = 3; // there are better ways to do this, but not faster ways, and it should not be done at all anyway...
	static public final boolean concurrency = false;
	static public final boolean demo = true;

}
