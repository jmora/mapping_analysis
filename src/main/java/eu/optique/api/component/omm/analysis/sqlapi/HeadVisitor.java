package eu.optique.api.component.omm.analysis.sqlapi;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnalyticExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.CastExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.ExtractExpression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.IntervalExpression;
import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.JsonExpression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.OracleHierarchicalExpression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.SignedExpression;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Modulo;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.expression.operators.relational.RegExpMatchOperator;
import net.sf.jsqlparser.expression.operators.relational.RegExpMySQLOperator;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.create.index.CreateIndex;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.view.CreateView;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.execute.Execute;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.jsqlparser.statement.select.SetOperationList;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.WithItem;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;
import eu.optique.api.component.omm.analysis.sqlapi.SelectVariableCaseClasses.Variable;

/**
 * @author jmora
 * 
 */
public class HeadVisitor implements StatementVisitor, SelectVisitor, SelectItemVisitor, ExpressionVisitor {

	private List<Variable> variables = new ArrayList<Variable>();

	public static List<Variable> getVariables (Statement query) {
		HeadVisitor visitor = new HeadVisitor();
		query.accept(visitor);
		return visitor.getVariables();
	}

	private List<Variable> getVariables () {
		return this.variables;
	}

	@Override
	public void visit (Select arg0) {
		HeadVisitor visitor = new HeadVisitor();
		arg0.getSelectBody().accept(visitor);
		this.variables = visitor.getVariables();
	}

	@Override
	public void visit (Delete arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Update arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Insert arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Replace arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Drop arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Truncate arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (CreateIndex arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (CreateTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (CreateView arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Alter arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (PlainSelect arg0) {
		this.variables = new ArrayList<Variable>();
		for (SelectItem si : arg0.getSelectItems()) {
			HeadVisitor visitor = new HeadVisitor();
			si.accept(visitor);
			this.variables.addAll(visitor.getVariables());
		}
	}

	@Override
	public void visit (SetOperationList arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (WithItem arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (AllColumns arg0) {
		this.variables.add(new Variable("*", "*"));
	}

	@Override
	public void visit (AllTableColumns arg0) {
		arg0.getTable().getAlias();
		this.variables.add(new Variable("*", "*"));
		// PROBLEM: This is wrong, but anyway, does anyone use this feature?
		// This is the SQL *Partial* API
	}

	@Override
	public void visit (SelectExpressionItem arg0) {
		HeadVisitor visitor = new HeadVisitor();
		arg0.getExpression().accept(visitor);
		ArrayList<String> groupOfVariables = new ArrayList<String>();
		for (Variable v : visitor.getVariables())
			groupOfVariables.addAll(v.getVariables());
		this.variables.add(new Variable(arg0.getAlias(), groupOfVariables));
	}

	public void visit2 (SelectExpressionItem arg0) {
		HeadVisitor visitor = new HeadVisitor();
		arg0.getExpression().accept(visitor);
		for (Variable v : visitor.getVariables())
			this.variables.add(new Variable(arg0.getAlias(), v.getVariables()));
	}

	@Override
	public void visit (NullValue arg0) {

	}

	@Override
	public void visit (Function arg0) {
		for (Expression e : arg0.getParameters().getExpressions())
			this.regularExpressionVisit(e);
	}

	@Override
	public void visit (JdbcParameter arg0) {
	}

	@Override
	public void visit (JdbcNamedParameter arg0) {
	}

	@Override
	public void visit (DoubleValue arg0) {
	}

	@Override
	public void visit (LongValue arg0) {
	}

	@Override
	public void visit (DateValue arg0) {
	}

	@Override
	public void visit (TimeValue arg0) {
	}

	@Override
	public void visit (TimestampValue arg0) {
	}

	@Override
	public void visit (Parenthesis arg0) {
		this.regularExpressionVisit(arg0.getExpression());
	}

	@Override
	public void visit (StringValue arg0) {
	}

	@Override
	public void visit (Addition arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (Division arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (Multiplication arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (Subtraction arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (AndExpression arg0) {
		this.binVisit(arg0);
	}

	public void binVisit (BinaryExpression arg0) {
		HeadVisitor visitor1 = new HeadVisitor();
		arg0.getLeftExpression().accept(visitor1);
		HeadVisitor visitor2 = new HeadVisitor();
		arg0.getRightExpression().accept(visitor2);
		this.variables.addAll(visitor1.getVariables());
		this.variables.addAll(visitor2.getVariables());
	}

	@Override
	public void visit (Between arg0) {
		HeadVisitor visitor1 = new HeadVisitor();
		arg0.getLeftExpression().accept(visitor1);
		this.variables.addAll(visitor1.getVariables());
	}

	@Override
	public void visit (EqualsTo arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (GreaterThan arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (GreaterThanEquals arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (InExpression arg0) {
		this.regularExpressionVisit(arg0.getLeftExpression());
	}

	@Override
	public void visit (IsNullExpression arg0) {
		this.regularExpressionVisit(arg0.getLeftExpression());
	}

	@Override
	public void visit (LikeExpression arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (MinorThan arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (MinorThanEquals arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (NotEqualsTo arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (Column arg0) {
		this.variables.add(new Variable("", arg0.getFullyQualifiedName()));
	}

	@Override
	public void visit (SubSelect arg0) {
		String alias = arg0.getAlias().getName();
		HeadVisitor visitor = new HeadVisitor();
		arg0.getSelectBody().accept(visitor);
		ArrayList<String> vars = new ArrayList<String>();
		for (Variable v : visitor.getVariables())
			vars.addAll(v.getVariables());
		this.variables.add(new Variable(alias, vars));
	}

	@Override
	public void visit (CaseExpression arg0) {
		this.regularExpressionVisit(arg0.getElseExpression());
		this.regularExpressionVisit(arg0.getSwitchExpression());
		for (Expression e : arg0.getWhenClauses())
			this.regularExpressionVisit(e);
	}

	@Override
	public void visit (WhenClause arg0) {
		this.regularExpressionVisit(arg0.getThenExpression());
		this.regularExpressionVisit(arg0.getWhenExpression());
	}

	@Override
	public void visit (ExistsExpression arg0) {
		this.regularExpressionVisit(arg0.getRightExpression());
	}

	@Override
	public void visit (AllComparisonExpression arg0) {
		this.regularExpressionVisit(arg0.getSubSelect());
	}

	private void regularExpressionVisit (Expression arg0) {
		HeadVisitor visitor = new HeadVisitor();
		arg0.accept(visitor);
		this.variables.addAll(visitor.getVariables());
	}

	@Override
	public void visit (AnyComparisonExpression arg0) {
		this.regularExpressionVisit(arg0.getSubSelect());
	}

	@Override
	public void visit (Concat arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (Matches arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (BitwiseAnd arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (BitwiseOr arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (BitwiseXor arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (CastExpression arg0) {
		this.regularExpressionVisit(arg0.getLeftExpression());
	}

	@Override
	public void visit (Modulo arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (AnalyticExpression arg0) {
		this.regularExpressionVisit(arg0.getExpression());
	}

	@Override
	public void visit (ExtractExpression arg0) {
		this.regularExpressionVisit(arg0.getExpression());
	}

	@Override
	public void visit (IntervalExpression arg0) {
	}

	@Override
	public void visit (OracleHierarchicalExpression arg0) {
		this.regularExpressionVisit(arg0.getConnectExpression());
		this.regularExpressionVisit(arg0.getStartExpression());
	}

	@Override
	public void visit (RegExpMatchOperator arg0) {
		this.binVisit(arg0);
	}

	@Override
	public void visit (OrExpression arg0) {
		this.binVisit(arg0);
	}

	public static List<Variable> getVariables (SelectBody subquery) {
		HeadVisitor visitor = new HeadVisitor();
		subquery.accept(visitor);
		return visitor.getVariables();
	}

	@Override
	public void visit (SignedExpression arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Statements arg0) {
		HeadVisitor visitor;
		for (Statement statement : arg0.getStatements()) {
			visitor = new HeadVisitor();
			statement.accept(visitor);
			this.variables.addAll(visitor.getVariables());
		}
	}

	@Override
	public void visit (JsonExpression arg0) {
		HeadVisitor visitor = new HeadVisitor();
		arg0.getColumn().accept(visitor);
		String alias = visitor.getVariables().get(0).getAlias();
		this.variables.add(new Variable(alias, arg0.getIdents()));
	}

	@Override
	public void visit (RegExpMySQLOperator arg0) {
		this.regularExpressionVisit(arg0.getLeftExpression());
		this.regularExpressionVisit(arg0.getRightExpression());
	}

	@Override
	public void visit (Execute arg0) {
		HeadVisitor visitor;
		for (Expression expr : arg0.getExprList().getExpressions()) {
			visitor = new HeadVisitor();
			expr.accept(visitor);
			this.variables.addAll(visitor.getVariables());
		}
	}
}
