package eu.optique.api.component.omm.analysis.sqlapi;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.create.index.CreateIndex;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.view.CreateView;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.execute.Execute;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.LateralSubSelect;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.jsqlparser.statement.select.SetOperationList;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.ValuesList;
import net.sf.jsqlparser.statement.select.WithItem;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;
import eu.optique.api.component.omm.analysis.sqlapi.FromTableCaseClasses.NamedTable;
import eu.optique.api.component.omm.analysis.sqlapi.FromTableCaseClasses.Source;
import eu.optique.api.component.omm.analysis.sqlapi.FromTableCaseClasses.Subselect;

/**
 * @author jmora
 * 
 */
public class BodyVisitor implements StatementVisitor, SelectVisitor, FromItemVisitor {

	private List<Source> bodyTables = new ArrayList<Source>();

	public void visit (Statement query) {
		BodyVisitor visitor = new BodyVisitor();
		query.accept(visitor);
		this.bodyTables.addAll(visitor.getBodyTables());
	}

	public static List<Source> getBodyTables (Statement query) {
		BodyVisitor visitor = new BodyVisitor();
		query.accept(visitor);
		return visitor.getBodyTables();
	}

	public List<Source> getBodyTables () {
		return this.bodyTables;
	}

	@Override
	public void visit (Select arg0) {
		BodyVisitor visitor = new BodyVisitor();
		arg0.getSelectBody().accept(visitor);
		this.bodyTables.addAll(visitor.getBodyTables());
	}

	@Override
	public void visit (Delete arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Update arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Insert arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Replace arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Drop arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Truncate arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (CreateIndex arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (CreateTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (CreateView arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Alter arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (PlainSelect arg0) {
		BodyVisitor visitor = new BodyVisitor();
		arg0.getFromItem().accept(visitor);
		if (arg0.getJoins() != null)
			for (Join join : arg0.getJoins())
				join.getRightItem().accept(visitor);
		this.bodyTables.addAll(visitor.getBodyTables());
	}

	@Override
	public void visit (SetOperationList arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (WithItem arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit (Table arg0) {
		this.bodyTables.add(new NamedTable(arg0.getName(), arg0.getAlias()));
	}

	@Override
	public void visit (SubSelect arg0) {
		this.bodyTables.add(new Subselect(arg0.getSelectBody(), arg0.getAlias()));
	}

	@Override
	public void visit (SubJoin arg0) {
		BodyVisitor visitor = new BodyVisitor();
		arg0.getLeft().accept(visitor);
		arg0.getJoin().getRightItem().accept(visitor);
		this.bodyTables.addAll(visitor.getBodyTables());
	}

	@Override
	public void visit (LateralSubSelect arg0) {
		BodyVisitor visitor = new BodyVisitor();
		arg0.getSubSelect().accept(visitor);
		this.bodyTables.addAll(visitor.getBodyTables());
		visitor = new BodyVisitor();
		arg0.getSubSelect().accept((ItemsListVisitor) visitor);
		this.bodyTables.addAll(visitor.getBodyTables());
	}

	@Override
	public void visit (ValuesList arg0) {
		// TODO Auto-generated method stub
	}

	public static List<Source> getBodyTables (SelectBody subquery) {
		BodyVisitor visitor = new BodyVisitor();
		subquery.accept(visitor);
		return visitor.getBodyTables();
	}

	@Override
	public void visit (Statements arg0) {
		BodyVisitor visitor;
		this.bodyTables = new ArrayList<Source>();
		for (Statement statement : arg0.getStatements()) {
			visitor = new BodyVisitor();
			statement.accept(visitor);
			this.bodyTables.addAll(visitor.getBodyTables());
		}
	}

	@Override
	public void visit (Execute arg0) {
		BodyVisitor visitor;
		this.bodyTables = new ArrayList<Source>();
		for (Expression expr : arg0.getExprList().getExpressions()) {
			visitor = new BodyVisitor();
			this.bodyTables.addAll(visitor.getBodyTables());
		}

	}

}
