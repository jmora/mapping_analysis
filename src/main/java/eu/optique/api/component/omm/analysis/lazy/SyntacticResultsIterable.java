/**
 * 
 */
package eu.optique.api.component.omm.analysis.lazy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;
import eu.optique.api.component.omm.analysis.interfaces.optique.SingleAnalyser;

/**
 * @author jmora
 * 
 */
public class SyntacticResultsIterable implements Iterable<List<HTMLSerializable>> {

	private ArrayList<SingleAnalyser> analysers;

	public SyntacticResultsIterable(ArrayList<SingleAnalyser> analysers) {
		this.analysers = analysers;
	}

	@Override
	public Iterator<List<HTMLSerializable>> iterator () {
		return new SyntacticResultsIterator(this.analysers.iterator());
	}

}
