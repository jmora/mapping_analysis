package eu.optique.api.component.omm.analysis.incremental;

import java.util.List;
import java.util.Map;

import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public interface HeadSubsumptionChecker {

	// I personally like this one
	public List<List<String>> headSubsumesAsLists (TriplesMap a, TriplesMap b);

	public Map<String, String> headSubsumes (TriplesMap a, TriplesMap b);

}
