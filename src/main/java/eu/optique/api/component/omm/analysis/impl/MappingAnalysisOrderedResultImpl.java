package eu.optique.api.component.omm.analysis.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import eu.optique.api.component.omm.analysis.MappingAnalysisOrderedResult;
import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.MappingAnalysisResult;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class MappingAnalysisOrderedResultImpl implements MappingAnalysisOrderedResult {

	private Set<Integer> subject;
	private MappingAnalysisRelation predicate;
	private Set<Integer> object;
	private ArrayList<TriplesMap> ordering;

	@Override
	public MappingAnalysisResult<TriplesMap> toResult () {
		return new MappingAnalysisResultImpl<TriplesMap>(this.toValue(this.subject), this.predicate, this.toValue(this.object));
	}

	private Set<TriplesMap> toValue (Set<Integer> indexes) {
		HashSet<TriplesMap> result = new HashSet<TriplesMap>();
		for (Integer i : indexes)
			result.add(this.ordering.get(i));
		return result;
	}

	@Override
	public Set<Integer> getSubject () {
		return this.subject;
	}

	@Override
	public MappingAnalysisRelation getPredicate () {
		return this.predicate;
	}

	@Override
	public Set<Integer> getObject () {
		return this.object;
	}

	public MappingAnalysisOrderedResultImpl(Set<Integer> subject, MappingAnalysisRelation predicate, Set<Integer> object, ArrayList<TriplesMap> ordering) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
		this.ordering = ordering;
	}

}
