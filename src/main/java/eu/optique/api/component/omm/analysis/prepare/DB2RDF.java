package eu.optique.api.component.omm.analysis.prepare;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;

import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;

import com.fluidops.iwb.datasource.metadata.impl.RelationalDatabaseSerializer;

/**
 * @author jmora
 * 
 */
public class DB2RDF {

	public static void main (String[] args) throws Exception {
		// Values here, sorry
		RelationalDatabaseSerializer srlzr = new RelationalDatabaseSerializer();
		URI metaDataURI = new URIImpl("http://www.optique-project.eu/mapping-analysis/base/");
		List<String> schemaNames = Arrays.asList("tester");
		String driverClass = "org.postgresql.Driver";
		String connString = "jdbc:postgresql://127.0.0.1:5432/test?user=tester&password=tester&ssl=true";
		String user = "tester";
		String pass = "tester";
		String host = "127.0.0.1";
		List<Statement> statements = srlzr.serializeRelationalSchemaToRdf(metaDataURI, schemaNames, driverClass, connString, user, pass, host);
		DB2RDF.save(statements, new File("database-schema.rdf"));
	}

	private static void save (List<Statement> statements, File file) throws RDFHandlerException, FileNotFoundException {
		Model myGraph = new TreeModel(statements);
		FileOutputStream out = new FileOutputStream(file);
		RDFFormat format = Rio.getWriterFormatForMIMEType("application/rdf+xml");
		Rio.write(myGraph, out, format);
	}

}
