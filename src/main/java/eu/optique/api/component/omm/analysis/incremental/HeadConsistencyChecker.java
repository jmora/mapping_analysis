package eu.optique.api.component.omm.analysis.incremental;

import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public interface HeadConsistencyChecker {
	public boolean isHeadConsistent (TriplesMap triplesMap);
}
