/**
 * 
 */
package eu.optique.api.component.omm.analysis.lazy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;
import eu.optique.api.component.omm.analysis.interfaces.optique.SingleAnalyser;

/**
 * @author jmora
 * 
 */
public class SubsumptionResultsIterator implements Iterator<List<HTMLSerializable>> {

	private ArrayList<SingleAnalyser> analysers;
	private int i;
	private int j;

	public SubsumptionResultsIterator(ArrayList<SingleAnalyser> analysers) {
		this.analysers = analysers;
		this.i = 1;
		this.j = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext () {
		if (this.i >= this.analysers.size() - 1 && this.j >= this.i - 1)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#next()
	 */
	@Override
	public List<HTMLSerializable> next () {
		SingleAnalyser analyser1 = this.analysers.get(this.i);
		SingleAnalyser analyser2 = this.analysers.get(this.j);
		if (analyser1 == null || analyser2 == null)
			return null;
		this.increment();
		return analyser1.bothDirectionsSubsumption(analyser2);
	}

	private void increment () {
		if (this.j != this.i - 1)
			this.j++;
		else {
			this.i++;
			this.j = 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove () {
		// TODO Auto-generated method stub

	}

}
