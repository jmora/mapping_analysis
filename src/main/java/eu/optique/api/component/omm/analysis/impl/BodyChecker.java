package eu.optique.api.component.omm.analysis.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fluidops.iwb.datasource.metadata.Schema;

import eu.optique.api.component.omm.analysis.convenience.FileLineIterator;
import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.exceptions.BodyCheckerException;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.config.OptiqueConfig;

/**
 * @author jmora
 */
public class BodyChecker {

	private final static String axioms = "fof(transless,axiom,(\n![X,Y,Z] : ((less(X,Y) & less(Z,X)) => less(Z,Y))\n)).\n"
			+ "fof(antisim,axiom,(\n![X,Y] : ((lesseq(X,Y) & lesseq(Y,X)) => (X=Y))\n)).\n" + "fof(irrefl1,axiom,(\n![X] : ~( less(X,X) )\n)).\n" + "fof(ref1,axiom,(\n![X] : ( lesseq(X,X) )\n)).\n"
			+ "fof(asym,axiom,(\n![X,Y] : (less(X,Y) => ~less(Y,X))\n)).\n" + "fof(dual1,axiom,(\n![X,Y] : (less(X,Y) <=> greater(Y,X))\n)).\n"
			+ "fof(dual2,axiom,(\n![X,Y] : (lesseq(X,Y) <=> greatereq(Y,X))\n)).\n" + "fof(refred,axiom,(\n![X,Y] : (less(X,Y) <=> (lesseq(X,Y) & (X != Y)))\n)).\n";

	private eu.optique.api.component.omm.analysis.impl.bodychecker.BodyChecker inner;

	public BodyChecker(List<Schema> schemata) {
		try {
			this.inner = new eu.optique.api.component.omm.analysis.impl.bodychecker.BodyChecker(schemata);
			this.initializeInner();
		} catch (BodyCheckerException e) {
			this.inner = new ExceptionalBodyChecker(e);
		}
	}

	public BodyChecker(File schema) {
		try {
			this.inner = new eu.optique.api.component.omm.analysis.impl.bodychecker.BodyChecker();
			try (FileLineIterator file = new FileLineIterator(schema)) {
				this.inner.setSchema(Statics.mkstring(file, "\n", "", ""));
				this.initializeInner();
			} catch (IOException e) {
				this.inner = new ExceptionalBodyChecker(new BodyCheckerException(e));
			}
		} catch (BodyCheckerException e) {
			this.inner = new ExceptionalBodyChecker(e);
		}
	}

	private void initializeInner () throws BodyCheckerException {
		try {
			this.inner.setAxiomsPath(Statics.makeFile(BodyChecker.axioms, "Axioms", "ax").getPath());
			this.inner.setFilePath(File.createTempFile("conjecture", ".tmp").getPath());
			this.inner.setMemorylimit(1024);
			this.inner.setTheoremProverPath(OptiqueConfig.getConfig().getEproverPath());
			this.inner.setTimeout(30);
		} catch (IOException e) {
			throw new BodyCheckerException(e);
		}
	}

	public boolean subsumes (TriplesMap body1, TriplesMap body2, Map<String, String> renaming) throws BodyCheckerException {
		return this.inner.checkBodySubsumption(body2, body1, renaming);
	}

	public boolean isBodyConsistent (TriplesMap body) throws BodyCheckerException {
		return this.inner.checkBodyConsistency(body);
	}

}