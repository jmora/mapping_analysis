package eu.optique.api.component.omm.analysis;

import java.util.Set;

import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;

/**
 * @author jmora
 * 
 */
public interface MappingAnalysisSingleOrderedResult extends HTMLSerializable {

	public Set<Integer> getSubject ();

	public MappingAnalysisRelation getPredicate ();

	public Set<String> getObject ();

	public MappingAnalysisResult<String> toResult ();
}
