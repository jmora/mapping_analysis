package eu.optique.api.component.omm.analysis.convenience;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author jmora
 * 
 */
public class PoolManager implements AutoCloseable {
	private final int cores = Runtime.getRuntime().availableProcessors();
	private final ExecutorService pool = Executors.newFixedThreadPool(this.cores);

	public ExecutorService getPool () {
		return this.pool;
	}

	@Override
	public void close () throws Exception {
		this.pool.shutdown();
	}

	public Future<?> submit (Callable<?> callable) {
		return this.pool.submit(callable);
	}

}
