/**
 * 
 */
package eu.optique.api.component.omm.analysis.lazy;

import java.util.Iterator;
import java.util.List;

import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;
import eu.optique.api.component.omm.analysis.interfaces.optique.SingleAnalyser;

/**
 * @author jmora
 * 
 */
public class ConsistencyResultsIterator implements Iterator<List<HTMLSerializable>> {

	private Iterator<SingleAnalyser> iterator;

	public ConsistencyResultsIterator(Iterator<SingleAnalyser> iterator) {
		this.iterator = iterator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext () {
		return this.iterator.hasNext();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#next()
	 */
	@Override
	public List<HTMLSerializable> next () {
		return this.iterator.next().getConsistencyErrors();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove () {
		// TODO Auto-generated method stub

	}

}
