/**
 * 
 */
package eu.optique.api.component.omm.analysis.sqlapi;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnalyticExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.CastExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.ExtractExpression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.IntervalExpression;
import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.JsonExpression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.OracleHierarchicalExpression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.SignedExpression;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Modulo;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.expression.operators.relational.RegExpMatchOperator;
import net.sf.jsqlparser.expression.operators.relational.RegExpMySQLOperator;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.create.index.CreateIndex;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.view.CreateView;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.execute.Execute;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.jsqlparser.statement.select.SetOperationList;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.WithItem;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;

/**
 * @author jmora
 * 
 */
public class OrderSelectItemsVisitor implements StatementVisitor, SelectVisitor {

	private List<String> order;

	private OrderSelectItemsVisitor(List<String> rightOrder) {
		this.order = rightOrder; // use the static method
	}

	public static String orderSelectItems (String sqlQuery, List<String> order) throws JSQLParserException {
		return new OrderSelectItemsVisitor(order).getOrderedQuery(sqlQuery);
	}

	public String getOrderedQuery (String sqlQuery) throws JSQLParserException {
		Statement query = new CCJSqlParserManager().parse(new StringReader(sqlQuery));
		query.accept(this);
		return query.toString();
	}

	@Override
	public void visit (Select arg0) {
		arg0.getSelectBody().accept(this);
	}

	@Override
	public void visit (Delete arg0) {
	}

	@Override
	public void visit (Update arg0) {
	}

	@Override
	public void visit (Insert arg0) {
	}

	@Override
	public void visit (Replace arg0) {
	}

	@Override
	public void visit (Drop arg0) {
	}

	@Override
	public void visit (Truncate arg0) {
	}

	@Override
	public void visit (CreateIndex arg0) {
	}

	@Override
	public void visit (CreateTable arg0) {
	}

	@Override
	public void visit (CreateView arg0) {
	}

	@Override
	public void visit (Alter arg0) {
	}

	@Override
	public void visit (Statements arg0) {
	}

	@Override
	public void visit (Execute arg0) {
	}

	@Override
	public void visit (PlainSelect arg0) {
		arg0.setSelectItems(this.orderList(arg0.getSelectItems()));
	}

	private List<SelectItem> orderList (List<SelectItem> selectItems) {
		ArrayList<SelectItem> res = new ArrayList<SelectItem>();
		for (String name : this.order) {
			// TODO: this is not the most efficient way, but honestly, I still don't know what am I doing and what is a SelectItem
			for (int i = 0; i < selectItems.size(); i++) {
				SelectItem sitem = selectItems.get(i);
				if (this.matchAlias(sitem, name)) {
					res.add(sitem);
					selectItems.remove(i);
					break;
				}
			}
			for (int i = 0; i < selectItems.size(); i++) {
				SelectItem sitem = selectItems.get(i);
				if (this.matchName(sitem, name)) {
					res.add(sitem);
					selectItems.remove(i);
					break;
				}
			}
		}
		res.addAll(selectItems);
		return res;
	}

	private boolean matchAlias (SelectItem sitem, String name) {
		MySelectItemVisitor v = new MySelectItemVisitor();
		sitem.accept(v);
		return name.equalsIgnoreCase(v.getAlias());
	}

	private boolean matchName (SelectItem sitem, String name) {
		MySelectItemVisitor v = new MySelectItemVisitor();
		sitem.accept(v);
		return name.equalsIgnoreCase(v.getName());
	}

	private class MySelectItemVisitor implements SelectItemVisitor, ExpressionVisitor {
		private String alias = null;
		private String name = null;

		@Override
		public void visit (AllColumns arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (AllTableColumns arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (SelectExpressionItem arg0) {
			arg0.getExpression().accept(this); // this is gonna hurt...
			this.alias = arg0.getAlias().getName();
		}

		public String getAlias () { // so much for Maybe..
			if (this.alias != null)
				return this.alias;
			return "";
		}

		public String getName () {
			if (this.name != null)
				return this.name;
			return "";
		}

		@Override
		public void visit (NullValue arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Function arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (SignedExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (JdbcParameter arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (JdbcNamedParameter arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (DoubleValue arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (LongValue arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (DateValue arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (TimeValue arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (TimestampValue arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Parenthesis arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (StringValue arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Addition arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Division arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Multiplication arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Subtraction arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (AndExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (OrExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Between arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (EqualsTo arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (GreaterThan arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (GreaterThanEquals arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (InExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (IsNullExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (LikeExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (MinorThan arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (MinorThanEquals arg0) {
		}

		@Override
		public void visit (NotEqualsTo arg0) {
		}

		@Override
		public void visit (Column arg0) {
			this.name = arg0.getColumnName();
		}

		@Override
		public void visit (SubSelect arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (CaseExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (WhenClause arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (ExistsExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (AllComparisonExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (AnyComparisonExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Concat arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Matches arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (BitwiseAnd arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (BitwiseOr arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (BitwiseXor arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (CastExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (Modulo arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (AnalyticExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (ExtractExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (IntervalExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (OracleHierarchicalExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (RegExpMatchOperator arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (JsonExpression arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit (RegExpMySQLOperator arg0) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public void visit (SetOperationList arg0) {
		this.myJavadoc(arg0);
	}

	@Override
	public void visit (WithItem arg0) {
		this.myJavadoc(arg0);

	}

	private void myJavadoc (Object o) {
		System.out.println(o.getClass().getName() + ": " + o.toString());
	}
}
