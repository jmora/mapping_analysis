package eu.optique.api.component.omm.analysis.sqlapi;

import java.util.Arrays;
import java.util.List;

import net.sf.jsqlparser.expression.Alias;
import eu.optique.api.component.omm.analysis.convenience.Statics;

/**
 * @author jmora
 * 
 */
public class SelectVariableCaseClasses {
	static public class Variable {
		private String alias;
		private List<String> variables;

		@Override
		public String toString () {
			return Statics.mkstring(this.variables, ", ", "{", "}") + "";
		}

		public Variable(Alias alias2, List<String> baseVariables) {
			this.init1(baseVariables);
			this.init2(alias2);
		}

		public Variable(Alias alias2, String... baseVariables) {
			this.init1(baseVariables);
			this.init2(alias2);
		}

		public Variable(String alias2, List<String> baseVariables) {
			this.init1(baseVariables);
			this.init2(alias2);
		}

		public Variable(String alias2, String... baseVariables) {
			this.init1(baseVariables);
			this.init2(alias2);
		}

		private void init1 (List<String> baseVariables) {
			this.variables = baseVariables;
		}

		private void init1 (String[] baseVariables) {
			this.variables = Arrays.asList(baseVariables);
		}

		private void init2 (String alias2) {
			if (alias2.equals(""))
				this.alias = this.mkstring(this.variables);
			else
				this.alias = alias2;
		}

		private void init2 (Alias alias2) {
			if (alias2 == null)
				this.init2("");
			else
				this.init2(alias2.getName());
		}

		public String getAlias () {
			return this.alias;
		}

		public List<String> getVariables () {
			return this.variables;
		}

		private String mkstring (List<String> things) {
			StringBuffer res = new StringBuffer();
			if (things.size() != 0)
				res = new StringBuffer(things.get(0));
			for (int i = 1; i < things.size(); i++)
				res.append(" " + things.get(i));
			return res.toString();
		}

	}
}
