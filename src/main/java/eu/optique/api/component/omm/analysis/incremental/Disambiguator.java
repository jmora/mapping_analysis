/**
 * 
 */
package eu.optique.api.component.omm.analysis.incremental;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;

import eu.optique.api.mapping.PredicateMap;
import eu.optique.api.mapping.PredicateObjectMap;
import eu.optique.api.mapping.TermMap;
import eu.optique.api.mapping.TriplesMap;
import eu.optique.api.mapping.impl.TemplateAccessor;

/**
 * @author jmora
 * 
 */
public class Disambiguator {
	private HashMap<IRI, OWLEntity> smallSignature;
	private HashMap<IRI, OWLClass> classesInSignature;
	private OWLOntology ontology;
	private Log log = LogFactory.getLog(Disambiguator.class);

	public Disambiguator(OWLOntology ontology) {
		this.ontology = ontology;
		this.smallSignature = new HashMap<IRI, OWLEntity>();
		this.classesInSignature = new HashMap<IRI, OWLClass>();
		for (OWLDataProperty dataProperty : this.ontology.getDataPropertiesInSignature())
			this.smallSignature.put(dataProperty.getIRI(), dataProperty);
		for (OWLObjectProperty objectProperty : this.ontology.getObjectPropertiesInSignature())
			this.smallSignature.put(objectProperty.getIRI(), objectProperty);
		for (OWLClass owlClass : this.ontology.getClassesInSignature())
			this.classesInSignature.put(owlClass.getIRI(), owlClass);
	}

	public Set<OWLEntity> getRolesAndAttributes (TriplesMap triplesMap) {
		Set<OWLEntity> res = new HashSet<OWLEntity>();
		for (PredicateObjectMap predicateObjectMap : triplesMap.getPredicateObjectMaps())
			for (PredicateMap predicateMap : predicateObjectMap.getPredicateMaps())
				res.add(this.getProperty(predicateMap));
		return res;
	}

	public OWLEntity getProperty (TermMap resource) {
		IRI resourceIRI = new TemplateAccessor(resource).getAsIRIsh();
		OWLEntity res = this.smallSignature.get(resourceIRI);
		if (res == null)
			this.log.warn("Property ignored for the semantic analysis, it was not found in the ontology signature: " + resourceIRI.toString());
		return res;
	}

	public ArrayList<OWLClass> getAllClasses (TriplesMap triplesMap) { // TODO: Better test this because it may miserably fail...
		ArrayList<OWLClass> res = new ArrayList<OWLClass>();
		for (Object resource : triplesMap.getSubjectMap().getClasses(Object.class)) {
			IRI classIRI = IRI.create(resource.toString());
			if (this.classesInSignature.containsKey(classIRI))
				res.add(this.classesInSignature.get(classIRI));
			else
				this.log.warn("Class ignored for the semantic analysis, it was not found in the ontology signature: " + resource.toString());
		}
		return res;
	}

}
