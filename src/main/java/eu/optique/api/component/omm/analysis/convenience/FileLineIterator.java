/**
 * 
 */
package eu.optique.api.component.omm.analysis.convenience;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

/**
 * @author jmora
 * 
 */
public class FileLineIterator implements Iterable<String>, Iterator<String>, Closeable {

	private String line = null;
	private BufferedReader reader;

	public FileLineIterator(File file) {
		try {
			this.reader = new BufferedReader(new FileReader(file));
			this.line = this.reader.readLine();
		} catch (IOException e) {
			this.line = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext () {
		return this.line != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#next()
	 */
	@Override
	public String next () {
		String tline = this.line;
		try {
			this.line = this.reader.readLine();
		} catch (IOException e) {
			this.line = null;
		}
		return tline;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove () {
		// TODO Auto-generated method stub

	}

	@Override
	public void close () throws IOException {
		this.reader.close();
	}

	@Override
	public Iterator<String> iterator () {
		return this;
	}

}
