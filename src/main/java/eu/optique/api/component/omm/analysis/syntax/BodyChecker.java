package eu.optique.api.component.omm.analysis.syntax;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import net.sf.jsqlparser.statement.select.SelectBody;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedSingleResult;
import eu.optique.api.component.omm.analysis.sqlapi.FromTableCaseClasses.Source;
import eu.optique.api.component.omm.analysis.sqlapi.FromTableCaseClasses.SourceClass;
import eu.optique.api.component.omm.analysis.sqlapi.SQLPartialAPI;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class BodyChecker {

	private SQLPartialAPI spapi;
	private HashMap<String, Set<String>> signature;
	private List<String> frontierVariables;
	private HashMap<String, ArrayList<String>> usedVariables;
	private HashMap<String, ArrayList<String>> availableVariables;
	private ArrayList<MappingAnalysisOrderedSingleResult> problems;
	private ArrayList<PartialProblem> partialProblems;
	private HashMap<String, BodyChecker> children;
	private boolean illDefinition = false;
	private ArrayList<TriplesMap> mappings;
	private Set<Integer> mapping;
	private HashSet<String> undefinedTables = new HashSet<String>();
	private Log log = LogFactory.getLog(BodyChecker.class);

	private class PartialProblem {
		private MappingAnalysisRelation relation;
		private HashSet<String> problem;

		public PartialProblem(MappingAnalysisRelation relation, String problem) {
			this.relation = relation;
			this.problem = new HashSet<String>(Arrays.asList(problem));
		}

		public PartialProblem(MappingAnalysisRelation relation, HashSet<String> problem) {
			this.relation = relation;
			this.problem = problem;
		}

		public MappingAnalysisRelation getRelation () {
			return this.relation;
		}

		public HashSet<String> getProblem () {
			return this.problem;
		}

	}

	public BodyChecker(SQLPartialAPI query, HashMap<String, Set<String>> signature, Set<Integer> mapping, ArrayList<TriplesMap> mappings) {
		this.spapi = query;
		this.signature = signature;
		this.mapping = mapping;
		this.mappings = mappings;
		this.init(); // heavy constructor, because it's going to save so much null checking...
	}

	public BodyChecker(SelectBody query, HashMap<String, Set<String>> signature, Set<Integer> mapping, ArrayList<TriplesMap> mappings) {
		this.mapping = mapping;
		this.mappings = mappings;
		this.spapi = new SQLPartialAPI(query);
		this.signature = signature;
		this.init();
	}

	protected void init () {
		this.problems = new ArrayList<MappingAnalysisOrderedSingleResult>();
		this.partialProblems = new ArrayList<PartialProblem>();
		this.children = this.calculateChildren();
		this.availableVariables = new HashMap<String, ArrayList<String>>();
		this.frontierVariables = new ArrayList<String>();
		this.usedVariables = new HashMap<String, ArrayList<String>>();
		// just try...
		try {
			this.availableVariables = this.calculateAvailableVariables();
		} catch (Exception e) {
		}
		try {
			this.frontierVariables = this.calculateFrontierVariables();
		} catch (Exception e) {
		}
		try {
			this.usedVariables = this.calculateUsedVariables();
		} catch (Exception e) {
		}
		this.problems = this.calculateProblems();
	}

	private HashMap<String, BodyChecker> calculateChildren () {
		HashMap<String, BodyChecker> res = new HashMap<String, BodyChecker>();
		List<Source> sources = this.spapi.getBodyTables();
		HashSet<String> allAliases = new HashSet<String>();
		for (Source s : sources) {
			String alias = s.getAlias();
			if (allAliases.contains(alias)) {
				this.illDefinition = true;
				if (s.getSourceClass() == SourceClass.Subselect)
					this.partialProblems.add(new PartialProblem(MappingAnalysisRelation.AMBIGUOUS_ALIAS, alias));
			}
			allAliases.add(alias);
		}
		if (this.illDefinition)
			return res;
		for (Source s : sources) {
			if (s.getSourceClass() == SourceClass.Subselect) {
				BodyChecker child = new BodyChecker(s.getSubselect(), this.signature, this.mapping, this.mappings);
				this.illDefinition |= child.illDefinition;
				res.put(s.getAlias(), child);
			}
			if (s.getSourceClass() == SourceClass.Table && !this.signature.containsKey(s.getTableName().toLowerCase())) {
				this.undefinedTables.add(s.getTableName());
				this.illDefinition = true;
			}
		}
		return res;
	}

	private ArrayList<MappingAnalysisOrderedSingleResult> calculateProblems () {
		if (!this.illDefinition)
			this.calculatePartialProblems();
		ArrayList<MappingAnalysisOrderedSingleResult> res = new ArrayList<MappingAnalysisOrderedSingleResult>();
		for (Entry<String, BodyChecker> child : this.children.entrySet())
			res.addAll(child.getValue().getProblems());
		for (PartialProblem problem : this.partialProblems)
			res.add(this.getFullProblem(problem));
		return res;
	}

	private void calculatePartialProblems () {
		this.calculateUndefinedVariables();
		this.calculateAmbiguousVariables();
		this.calculateUndefinedTables();
	}

	private void calculateUndefinedTables () {
		if (!this.undefinedTables.isEmpty())
			this.partialProblems.add(new PartialProblem(MappingAnalysisRelation.TABLE_NOT_FOUND, this.undefinedTables));
	}

	private void calculateAmbiguousVariables () {
		HashSet<String> ambiguousVariables = new HashSet<String>();
		for (Entry<String, ArrayList<String>> variable : this.getUsedVariables().entrySet())
			if (variable.getValue().contains("") && this.getAvailableVariables().containsKey(variable.getKey()) && this.getAvailableVariables().get(variable.getKey()).size() > 1)
				ambiguousVariables.add(variable.getKey());
		if (!ambiguousVariables.isEmpty())
			this.partialProblems.add(new PartialProblem(MappingAnalysisRelation.AMBIGUOUS_VARIABLE, ambiguousVariables));
	}

	private void calculateUndefinedVariables () {
		HashSet<String> undefinedVariables = new HashSet<String>();
		String variablekey;
		for (Entry<String, ArrayList<String>> variable : this.getUsedVariables().entrySet()) {
			variablekey = variable.getKey().toLowerCase();
			if (!variablekey.equals("*") && !this.getAvailableVariables().containsKey(variablekey))
				undefinedVariables.add(variable.getKey());
			else
				for (String s : variable.getValue())
					if (!s.equals("") && !this.getAvailableVariables().get(variablekey).contains(s))
						undefinedVariables.add(s + "." + variable.getKey());
		}
		if (!undefinedVariables.isEmpty())
			this.partialProblems.add(new PartialProblem(MappingAnalysisRelation.UNDEFINED_VARIABLE, undefinedVariables));
	}

	private MappingAnalysisOrderedSingleResult getFullProblem (PartialProblem problem) {
		return new MappingAnalysisOrderedSingleResult(this.mapping, problem.getRelation(), problem.getProblem(), this.mappings);
	}

	private HashMap<String, ArrayList<String>> calculateAvailableVariables () {
		HashMap<String, ArrayList<String>> res = new HashMap<String, ArrayList<String>>();
		String tableName;
		for (Source s : this.spapi.getBodyTables())
			if (s.getSourceClass() == SourceClass.Table) {
				tableName = s.getTableName().toLowerCase();
				if (this.signature.containsKey(tableName))
					for (String v : this.signature.get(tableName))
						this.addTo(v, s.getAlias(), res);
				else
					this.partialProblems.add(new PartialProblem(MappingAnalysisRelation.TABLE_NOT_FOUND, s.getTableName()));
			}
		for (Entry<String, BodyChecker> child : this.children.entrySet())
			for (String v : child.getValue().getFrontierVariables())
				this.addTo(v, child.getKey(), res);
		return res;
	}

	private HashMap<String, ArrayList<String>> calculateUsedVariables () {
		HashMap<String, ArrayList<String>> res = new HashMap<String, ArrayList<String>>();
		for (String v : this.spapi.getInternalVariables()) {
			List<String> re = new ArrayList<String>(Arrays.asList(v.split("\\.")));
			if (re.size() == 1)
				re.add(0, "");
			if (re.size() > 2)
				re = Arrays.asList(re.get(0), Statics.mkstring(re.subList(1, re.size()), "", "", "."));
			this.addTo(re.get(1), re.get(0), res);
		}
		return res;
	}

	private void addTo (String key, String value, HashMap<String, ArrayList<String>> dictionary) {
		String ckey = Statics.correctCase(key);
		String cvalue = Statics.correctCase(value);
		if (!dictionary.containsKey(ckey))
			dictionary.put(ckey, new ArrayList<String>());
		dictionary.get(ckey).add(cvalue);
	}

	private List<String> calculateFrontierVariables () {
		List<String> candidate = this.spapi.getFrontierVariables();
		if (candidate.size() > 0 && !candidate.get(0).equals("*"))
			return Statics.correctCase(candidate);
		return Statics.correctCase(this.getAvailableVariables().keySet());
	}

	public List<String> getFrontierVariables () {
		return this.frontierVariables;
	}

	public HashMap<String, ArrayList<String>> getUsedVariables () {
		return this.usedVariables;
	}

	public HashMap<String, ArrayList<String>> getAvailableVariables () {
		return this.availableVariables;
	}

	protected ArrayList<MappingAnalysisOrderedSingleResult> getProblems () {
		return this.problems;
	}

	public ArrayList<MappingAnalysisOrderedSingleResult> check () {
		return this.getProblems();
	}

	public static ArrayList<MappingAnalysisOrderedSingleResult> check (ArrayList<TriplesMap> mappings, int index, HashMap<String, Set<String>> signature, SQLPartialAPI query) {
		BodyChecker bc = new BodyChecker(query, signature, new HashSet<Integer>(Arrays.asList(index)), mappings);
		return bc.getProblems();
	}

	public static BodyChecker makeBodyChecker (ArrayList<TriplesMap> triplesMaps, int index, HashMap<String, Set<String>> signature, SQLPartialAPI query) {
		return new BodyChecker(query, signature, new HashSet<Integer>(Arrays.asList(index)), triplesMaps);
	}

	@Override
	public String toString () {
		return "Body checker for: " + this.spapi.toString();
	}
}
