package eu.optique.api.component.omm.analysis.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import eu.optique.api.component.omm.analysis.MappingAnalysisRelation;
import eu.optique.api.component.omm.analysis.MappingAnalysisResult;
import eu.optique.api.component.omm.analysis.convenience.Globals;
import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 * @param <T>
 */
public class MappingAnalysisResultImpl<T> implements MappingAnalysisResult<T> {

	private Collection<TriplesMap> subject;
	private MappingAnalysisRelation predicate;
	private Collection<T> object;

	@Override
	public Collection<TriplesMap> getSubject () {
		return this.subject;
	}

	@Override
	public MappingAnalysisRelation getPredicate () {
		return this.predicate;
	}

	@Override
	public Collection<T> getObject () {
		return this.object;
	}

	public MappingAnalysisResultImpl(Collection<TriplesMap> subject, MappingAnalysisRelation predicate, Collection<T> object) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}

	public Set<String> toLinks (Collection<TriplesMap> subject) {
		HashSet<String> result = new HashSet<String>();
		for (TriplesMap triplesMap : subject)
			result.add(Statics.triplesMapToAhref(triplesMap));
		return result;
	}

	private boolean checkIfTriplesMap (Collection<T> t) {
		if (t.isEmpty())
			return false;
		T tt = t.iterator().next();
		return tt instanceof TriplesMap;
	}

	@SuppressWarnings("unchecked")
	public Set<String> toLinksMaybe (Collection<T> collection) {
		if (this.checkIfTriplesMap(collection))
			return this.toLinks((Collection<TriplesMap>) collection);
		else
			return this.toStringAll(collection);
	}

	@SuppressWarnings("unchecked")
	private Set<String> toStringAll (Collection<T> collection) {
		if (this.checkIfTriplesMap(collection))
			return this.toNames((Collection<TriplesMap>) collection);
		HashSet<String> result = new HashSet<String>();
		for (T object : collection)
			result.add(object.toString());
		return result;
	}

	@Override
	public String toHTML () {
		return Statics.mkstring(this.toLinks(this.subject), ", ", "{", "} ") + this.predicate.message() + Statics.mkstring(this.toLinksMaybe(this.object), ", ", "{", "}");
	}

	@Override
	public String toNamedString () {
		return Statics.mkstring(this.toNames(this.subject), ", ", "{", "} ") + this.predicate.message() + Statics.mkstring(this.toStringAll(this.object), ", ", " {", "}");
	}

	public Set<String> toNames (Collection<TriplesMap> mappings) {
		HashSet<String> result = new HashSet<String>();
		for (TriplesMap mapping : mappings)
			try {
				result.add(mapping.getResource(Globals.sesameResource).getLocalName().toString());
			} catch (Exception e) {
				result.add(mapping.getResource(Globals.sesameAnonymousResource).toString());
			}
		return result;
	}

}
