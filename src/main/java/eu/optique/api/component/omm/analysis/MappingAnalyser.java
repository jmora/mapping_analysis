package eu.optique.api.component.omm.analysis;

import java.util.Collection;
import java.util.List;

import org.semanticweb.owlapi.model.OWLOntology;

import com.fluidops.iwb.datasource.metadata.Schema;

import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public interface MappingAnalyser {

	Collection<MappingAnalysisResult<TriplesMap>> analyse (Collection<TriplesMap> mappings, List<Schema> schema, OWLOntology ontology);

}
