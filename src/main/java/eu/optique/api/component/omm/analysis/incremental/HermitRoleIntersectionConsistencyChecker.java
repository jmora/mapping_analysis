/**
 * 
 */
package eu.optique.api.component.omm.analysis.incremental;

import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class HermitRoleIntersectionConsistencyChecker implements HeadConsistencyChecker {

	private RoleIntersectionConjunctor conjunctor;
	private OntologyAndReasonerWrap ontologyAndReasonerWrap;

	public HermitRoleIntersectionConsistencyChecker(RoleIntersectionConjunctor conjunctor, OntologyAndReasonerWrap ontologyAndReasonerWrap) {
		this.conjunctor = conjunctor;
		this.ontologyAndReasonerWrap = ontologyAndReasonerWrap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.optique.api.component.omm.analysis.incremental.HeadConsistencyChecker#isHeadConsistent(eu.optique.api.mapping.TriplesMap)
	 */
	@Override
	public boolean isHeadConsistent (TriplesMap triplesMap) {
		return this.ontologyAndReasonerWrap.isSatisfiable(this.conjunctor.getConjunction(triplesMap));
	}

}
