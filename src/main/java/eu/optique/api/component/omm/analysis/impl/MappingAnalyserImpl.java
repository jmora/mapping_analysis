/**
 * 
 */
package eu.optique.api.component.omm.analysis.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.semanticweb.owlapi.model.OWLOntology;

import com.fluidops.iwb.datasource.metadata.Schema;

import eu.optique.api.component.omm.analysis.convenience.Statics;
import eu.optique.api.component.omm.analysis.exceptions.BodyCheckerException;
import eu.optique.api.component.omm.analysis.incremental.Disambiguator;
import eu.optique.api.component.omm.analysis.incremental.HermitRoleIntersectionConsistencyChecker;
import eu.optique.api.component.omm.analysis.incremental.HermitRoleIntersectionSubsumptionChecker;
import eu.optique.api.component.omm.analysis.incremental.OntologyAndReasonerWrap;
import eu.optique.api.component.omm.analysis.incremental.RoleIntersectionConjunctor;
import eu.optique.api.component.omm.analysis.interfaces.optique.HTMLSerializable;
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class MappingAnalyserImpl {
	private List<Schema> schema;
	private OWLOntology ontology;
	private HermitRoleIntersectionConsistencyChecker headConsistencyChecker;
	private HermitRoleIntersectionSubsumptionChecker headSubsumptionChecker;
	private BodyChecker bodyChecker;

	public MappingAnalyserImpl(List<Schema> schemata, OWLOntology ontology) {
		this.schema = schemata;
		this.ontology = Statics.copyOntology(ontology);
		Disambiguator disambiguator = new Disambiguator(this.ontology);
		OntologyAndReasonerWrap ontologyAndReasonerWrap = new OntologyAndReasonerWrap(this.ontology);
		RoleIntersectionConjunctor iconjunctor = new RoleIntersectionConjunctor(ontologyAndReasonerWrap, disambiguator);
		this.headConsistencyChecker = new HermitRoleIntersectionConsistencyChecker(iconjunctor, ontologyAndReasonerWrap);
		this.headSubsumptionChecker = new HermitRoleIntersectionSubsumptionChecker(iconjunctor, ontologyAndReasonerWrap);
		this.headSubsumptionChecker = new HermitRoleIntersectionSubsumptionChecker(iconjunctor, ontologyAndReasonerWrap);
		this.bodyChecker = new BodyChecker(schemata);
	}

	// If empty then no syntactic errors
	public List<HTMLSerializable> syntacticAnalysis (TriplesMap triplesMap) {
		return SyntacticAnalyserImpl.analyse(new ArrayList<TriplesMap>(Arrays.asList(triplesMap)), this.schema, this.ontology);
	}

	public boolean isSyntacticallyCorrect (TriplesMap triplesMap) {
		return this.syntacticAnalysis(triplesMap).isEmpty();
	}

	public boolean isHeadConsistent (TriplesMap triplesMap) {
		return this.headConsistencyChecker.isHeadConsistent(triplesMap);
	}

	public boolean isBodyConsistent (TriplesMap triplesMap) throws BodyCheckerException {
		return this.bodyChecker.isBodyConsistent(triplesMap);
	}

	public boolean isConsistent (TriplesMap triplesMap) throws BodyCheckerException {
		return this.isHeadConsistent(triplesMap) && this.isBodyConsistent(triplesMap);
	}

	public boolean headSubsumes (TriplesMap triplesMap1, TriplesMap triplesMap2) {
		return this.headSubsumptionChecker.headSubsumes(triplesMap1, triplesMap2) != null;
	}

	private Map<String, String> headSubsumption (TriplesMap triplesMap1, TriplesMap triplesMap2) {
		return this.headSubsumptionChecker.headSubsumes(triplesMap1, triplesMap2);
	}

	public boolean bodySubsumes (TriplesMap triplesMap1, TriplesMap triplesMap2) throws BodyCheckerException {
		Map<String, String> variableRenaming = this.headSubsumption(triplesMap2, triplesMap1);
		if (variableRenaming == null)
			variableRenaming = this.headSubsumption(triplesMap1, triplesMap2);
		if (variableRenaming == null)
			return false;
		return this.bodyChecker.subsumes(triplesMap1, triplesMap2, variableRenaming);
	}

	public boolean subsumes (TriplesMap triplesMap1, TriplesMap triplesMap2) throws BodyCheckerException {
		Map<String, String> variableRenaming = this.headSubsumption(triplesMap2, triplesMap1);
		if (variableRenaming == null)
			return false;
		return this.bodyChecker.subsumes(triplesMap1, triplesMap2, variableRenaming);
	}

}
