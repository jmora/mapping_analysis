package eu.optique.api.component.omm.analysis.convenience;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.util.AddAliasesVisitor;

import org.apache.commons.collections.IteratorUtils;
import org.coode.owlapi.turtle.TurtleOntologyFormat;
import org.openrdf.model.Resource;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import com.fluidops.iwb.api.EndpointImpl;
import com.fluidops.iwb.datasource.metadata.Schema;
import com.google.common.collect.Maps;

import eu.optique.api.mapping.ManageResource;
import eu.optique.api.mapping.TriplesMap;

/**
 * @author jmora
 * 
 */
public class Statics { // because implicits in Java are "hard"

	public static String mkstring (Iterable<String> parts) {
		return Statics.mkstring(parts, ", ", "[ ", " ]");
	}

	public static String mkstring (Iterable<String> parts, String sep, String start, String end) {
		StringBuffer res = new StringBuffer(start);
		Iterator<String> piterator = parts.iterator();
		if (piterator.hasNext())
			res.append(piterator.next());
		while (piterator.hasNext())
			res.append(sep + piterator.next());
		res.append(end);
		return res.toString();
	}

	public static String mkstringobj (Iterable<? extends Object> parts, String sep, String start, String end) {
		StringBuffer res = new StringBuffer(start);
		Iterator<? extends Object> piterator = parts.iterator();
		if (piterator.hasNext())
			res.append(piterator.next().toString());
		while (piterator.hasNext())
			res.append(sep + piterator.next().toString());
		res.append(end);
		return res.toString();
	}

	public static List<String> splitAtLast (String string, String sep) {
		int i = string.lastIndexOf(sep);
		if (i == -1)
			return Arrays.asList(string);
		return Arrays.asList(string.substring(0, i), string.substring(i + 1, string.length()));
	}

	public static String correctCase (String thing) {
		if (Globals.caseinsensitiveness)
			return thing.toLowerCase();
		else
			return thing;
	}

	public static ArrayList<String> correctCase (Collection<String> things) {
		if (Globals.caseinsensitiveness) {
			List<String> l = IteratorUtils.toList(new IterableCorrectCase(things).iterator());
			return new ArrayList<String>(l);
		} else
			return new ArrayList<String>(things);
	}

	public static String sesameResourceToURI (ManageResource resource) {
		String res = "";
		try {
			res = resource.getResource(Globals.sesameResource).getLocalName().toString();
		} catch (Exception e) {
			res = resource.getResource(Globals.sesameAnonymousResource).toString();
		}
		return res;
	}

	public static IRI sesameResourceToIRI (ManageResource resource) {
		String res = "";
		try {
			res = resource.getResource(Globals.sesameResource).toString();
		} catch (Exception e) {
			res = resource.getResource(Globals.sesameAnonymousResource).toString();
		}
		return IRI.create(res);
	}

	public static Resource sesameResourceToResource (ManageResource resource) { // loving this method
		try {
			return resource.getResource(Globals.sesameResource);
		} catch (Exception e) {
			return resource.getResource(Globals.sesameAnonymousResource);
		}
	}

	public static String triplesMapToAhref (TriplesMap map) {
		Map<String, String> linkParams = Maps.newHashMap();
		linkParams.put("target", "_blank");
		Resource resource = Statics.sesameResourceToResource(map);
		String label = EndpointImpl.api().getDataManager().getLabel(resource);
		return EndpointImpl.api().getRequestMapper().getAHrefEncoded(resource, label, "Click to open mapping rule in new tab.", linkParams);
	}

	public static void saveOntology (OWLOntology ontology, String filepath) {
		File outputFile = new File(filepath);
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		manager.setOntologyFormat(ontology, new TurtleOntologyFormat());
		try {
			manager.saveOntology(ontology, IRI.create(outputFile.toURI()));
		} catch (OWLOntologyStorageException e) {
			e.printStackTrace();
		}
	}

	static public String repairSQLQuery (String querySQL) throws JSQLParserException {
		Select select = (Select) CCJSqlParserUtil.parse(querySQL);
		final AddAliasesVisitor instance = new AddAliasesVisitor();
		select.getSelectBody().accept(instance);
		return select.toString();
	}

	static public OWLOntology copyOntology (OWLOntology original) {
		try {
			return OWLManager.createOWLOntologyManager().createOntology(original.getAxioms());
		} catch (OWLOntologyCreationException e) {
			return original;
		}
	}

	public static Schema getBiggest (List<Schema> schema) {
		if (schema.size() == 1)
			return schema.get(0);
		if (schema.size() < 1)
			return null;
		Schema res = schema.get(0);
		for (Schema s : schema)
			if (s.getTables().size() > res.getTables().size())
				res = s;
		return res;
	}

	public static String mkstring (Object[] things, String sep, String start, String end) {
		ArrayList<String> t = new ArrayList<String>();
		for (Object o : things)
			t.add(o.toString());
		return Statics.mkstring(t, sep, start, end);
	}

	public static File makeFile (String contents, String name, String extension) throws IOException {
		File file = File.createTempFile(name, extension);
		try (PrintWriter writer = new PrintWriter(file)) {
			writer.print(contents);
		}
		return file;
	}
}
