package eu.optique.api.component.omm.analysis.syntax;

import java.util.Set;

/**
 * @author jmora
 * 
 */
class Result {
	String key;

	public String getKey () {
		return this.key;
	}

	public void setKey (String key) {
		this.key = key;
	}

	public Set<String> getValue () {
		return this.value;
	}

	public void setValue (Set<String> value) {
		this.value = value;
	}

	Set<String> value;

	public Result(String key, Set<String> value) {
		this.key = key;
		this.value = value;
	}
}
