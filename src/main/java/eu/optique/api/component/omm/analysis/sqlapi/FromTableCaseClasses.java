package eu.optique.api.component.omm.analysis.sqlapi;

import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.statement.select.SelectBody;

/**
 * @author jmora
 * 
 */
public class FromTableCaseClasses {
	static public enum SourceClass {
		Subselect, Table
	}

	static abstract public class Source {
		protected String alias;
		protected SelectBody subselect = null;
		protected String name = "";

		public abstract SourceClass getSourceClass ();

		public SelectBody getSubselect () {
			return this.subselect;
		}

		public String getTableName () {
			return this.name;
		}

		protected void setAlias (Alias alias2) {
			if (alias2 != null)
				this.alias = alias2.getName();
			if (this.alias == null || this.alias.equals(""))
				this.alias = this.name;
			if (this.name == null || this.name.equals(""))
				this.name = this.alias;
		}

		public String getAlias () {
			return this.alias;
		}

		@Override
		public String toString () {
			if (this.getSourceClass() == SourceClass.Subselect)
				return this.subselect.toString();
			if (this.getSourceClass() == SourceClass.Table)
				return this.getTableName() + " as " + this.getAlias();
			return "";
		}
	};

	static public class Subselect extends Source {

		public Subselect(SelectBody selectBody, Alias alias) {
			this.subselect = selectBody;
			this.setAlias(alias);
		}

		@Override
		public SourceClass getSourceClass () {
			return SourceClass.Subselect;
		}

	}

	static public class NamedTable extends Source {

		public NamedTable(String name, Alias alias) {
			this.name = name;
			this.setAlias(alias);
		}

		@Override
		public SourceClass getSourceClass () {
			return SourceClass.Table;
		}

	}
}
