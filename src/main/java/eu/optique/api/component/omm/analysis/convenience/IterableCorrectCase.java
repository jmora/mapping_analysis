package eu.optique.api.component.omm.analysis.convenience;

import java.util.Iterator;

/**
 * @author jmora
 * 
 */
public class IterableCorrectCase implements Iterable<String> {

	private Iterable<String> strings;

	public IterableCorrectCase(Iterable<String> things) {
		this.strings = things;
	}

	@Override
	public Iterator<String> iterator () {
		return new IteratorCorrectCase(this.strings.iterator());
	}

	public class IteratorCorrectCase implements Iterator<String> {
		private Iterator<String> innerIterator;

		public IteratorCorrectCase(Iterator<String> iterator) {
			this.innerIterator = iterator;
		}

		@Override
		public boolean hasNext () {
			return this.innerIterator.hasNext();
		}

		@Override
		public String next () {
			return this.innerIterator.next().toLowerCase();
		}

		@Override
		public void remove () {
			this.innerIterator.remove();
		}

	}

}
