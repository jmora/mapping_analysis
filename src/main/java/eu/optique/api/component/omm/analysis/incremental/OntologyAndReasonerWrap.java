/**
 * 
 */
package eu.optique.api.component.omm.analysis.incremental;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import eu.optique.api.component.omm.analysis.convenience.Globals;

/**
 * @author jmora
 * 
 */
public class OntologyAndReasonerWrap {

	private OWLOntology ontology;
	private OWLReasoner reasoner;
	private OWLOntologyManager owlOntologyManager;
	private boolean needsPrecompute;

	public OntologyAndReasonerWrap(OWLOntology ontology) {
		this.ontology = ontology;
		this.reasoner = Globals.reasonerFactory.createReasoner(this.ontology);
		this.owlOntologyManager = OWLManager.createOWLOntologyManager();
		this.needsPrecompute = true;
	}

	public void addAxiom (OWLAxiom axiom) {
		this.owlOntologyManager.addAxiom(this.ontology, axiom);
		this.needsPrecompute = true;
	}

	public boolean isSatisfiable (OWLClassExpression owlClass) {
		this.precomputeIfNeeded();
		return this.reasoner.isSatisfiable(owlClass);
	}

	private void precomputeIfNeeded () {
		if (!this.needsPrecompute)
			return;
		this.reasoner = Globals.reasonerFactory.createReasoner(this.ontology);
		this.reasoner.precomputeInferences();
		this.needsPrecompute = false;
	}

	public NodeSet<OWLClass> getSubClasses (OWLClassExpression owlClassExpression) {
		this.precomputeIfNeeded();
		return this.reasoner.getSubClasses(owlClassExpression, false);
	}

	public NodeSet<OWLClass> getSuperClasses (OWLClassExpression owlClassExpression) {
		this.precomputeIfNeeded();
		return this.reasoner.getSuperClasses(owlClassExpression, false);
	}

	public boolean isSubclassOf (OWLClass subclass, OWLClass candidate) {
		this.precomputeIfNeeded();
		return this.reasoner.getSubClasses(candidate, false).containsEntity(subclass);
	}

	public Node<OWLClass> getEquivalentClasses (OWLClassExpression owlClassExpression) {
		this.precomputeIfNeeded();
		return this.reasoner.getEquivalentClasses(owlClassExpression);
	}
}
